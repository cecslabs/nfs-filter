/*
 * util.c - various useful fucnctions
 *
 * by Robert Edwards <bob@cs.anu.edu.au>, June 2014
 *
 * util.c is free software, licensed under the GPL version 2
 *
 * You can download the GNU General Public License from the GNU website
 * at http://www.gnu.org/ or write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#define _GNU_SOURCE
#include <stdio.h>
#include <syslog.h>

#include <ctype.h>
#include <fcntl.h>
#include <netdb.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

#include "util.h"

int timed_read (int sock, char *buf, int len, int secs) {
	int res;
	fd_set rfds;
	struct timeval timeout;

	FD_ZERO (&rfds);
	FD_SET (sock, &rfds);
	timeout.tv_sec = secs;
	timeout.tv_usec = 0;
	res = select (sock + 1, &rfds, NULL, NULL, &timeout);
	if (res <= 0) return res;
	return read (sock, buf, len);
} /* timed_read () */

int try_write (int sock, char *buf, int len) {
	int done = 0;
	while (len) {
		int num = write (sock, buf + done, len - done);
		if (num <= 0) return num;
		done += num;
		len -= num;
	}
	return (done);
}

struct sockaddr *get_sockaddr (struct sockaddr *addr, const char *canon) {
	sa_family_t family = addr->sa_family;

	if ((family == AF_UNIX) || (family == AF_LOCAL)) {
		struct sockaddr_un *sa = (struct sockaddr_un *)addr;
		memset (sa->sun_path, 0, sizeof(sa->sun_path));
		strncpy (sa->sun_path, canon, sizeof(sa->sun_path) - 1);
		debug_msg (LOG_DEBUG, "set up AF_UNIX socket");
	} else if ((family == AF_INET) || (family == AF_INET6)) {
		struct addrinfo hints;
		struct addrinfo *res;

		memset (&hints, 0, sizeof(struct addrinfo));
		hints.ai_family = family;

		if (getaddrinfo (canon, NULL, &hints, &res) != 0) {
			debug_msg (LOG_ERR, "getaddrinfo");
			return NULL;
		}
		memcpy (addr, res->ai_addr, res->ai_addrlen);
		freeaddrinfo (res);
		debug_msg (LOG_DEBUG, "set up AF_INET[6] socket");
	}
	return addr;
}

socklen_t sock_addrlen (const struct sockaddr *addr) {
	switch (addr->sa_family) {
		case AF_UNIX:	/* also covers AF_LOCAL */
			return sizeof (struct sockaddr_un);
		case AF_INET:
			return sizeof (struct sockaddr_in);
		case AF_INET6:
			return sizeof (struct sockaddr_in6);
		default:
			return -1;
	}
}

int tcp_connect (struct sockaddr *addr, int port) {
	int sock;

	if (addr == NULL) {
		debug_msg (LOG_DEBUG, "addr is NULL");
		return -1;
	}

	if ((sock = socket (addr->sa_family, SOCK_STREAM, 0)) < 0) {
		debug_msg (LOG_ERR, "socket");
		return -1;
	}

	/* should work for AF_INET and AF_INET6... */
	((struct sockaddr_in *)addr)->sin_port = htons(port);

	if (connect (sock, addr, sock_addrlen (addr)) < 0) {
		debug_msg (LOG_ERR, "connect");
		close (sock);
		return -1;
	}

	return sock;
}

int tcp_listen (int family, int port) {
	int sock;
	struct sockaddr_in sa;

	if ((sock = socket (family, SOCK_STREAM, 0)) < 0) {
		perror ("tcp socket");
		return -1;
	}

	sa.sin_family = AF_INET;
	sa.sin_port = htons(port);
	sa.sin_addr.s_addr = 0;

	if (bind (sock, (struct sockaddr *) &sa, sizeof (sa)) < 0) {
   		perror ("tcp bind");
		close (sock);
		return -1;
	}

	if (listen (sock, 5) == -1) {
		perror ("tcp listen");
		close (sock);
		return -1;
	}

	return sock;
}

int uds_connect (const char *sockpath) {
	int sock;
	struct sockaddr_un addr;
	struct sockaddr *sa = (struct sockaddr *) &addr;

	if (sockpath == NULL)
		return -1;

	if ((sock = socket (PF_UNIX, SOCK_STREAM|SOCK_CLOEXEC, 0)) < 0) {
		perror ("socket");
		return -1;
	}

	sa->sa_family = AF_UNIX;
	get_sockaddr (sa, sockpath);

	if (connect (sock, sa, SUN_LEN(&addr)) < 0) {
		perror ("connect");
		close (sock);
		return -1;
	}

	return sock;
}

int uds_listen (const char *sockpath, mode_t mode, int max_conn) {
	int sock;
	struct sockaddr_un addr;
	struct sockaddr *sa = (struct sockaddr *) &addr;

	if (sockpath == NULL)
		return -1;

	if ((sock = socket (PF_UNIX, SOCK_STREAM, 0)) < 0) {
		perror ("socket");
		return -1;
	}

	unlink (sockpath);

	/* make it non-blocking */
//	if (fcntl (sock, F_SETFL, fcntl (sock, F_GETFL, 0) | O_NONBLOCK) < 0) {
//		perror ("fcntl (F_SETFL)");
//		close (sock);
//		return -1;
//	}

	sa->sa_family = AF_UNIX;
	get_sockaddr (sa, sockpath);

	if (bind (sock, sa, SUN_LEN(&addr))) {
		debug_msg (LOG_ERR, "bind failed for %s", sockpath);
		close (sock);
		return -1;
	}

	/* close the file descriptor on exec */
	if (fcntl (sock, F_SETFD, FD_CLOEXEC) < 0) {
		perror ("fcntl (F_SETFD, FD_CLOEXEC)");
		close (sock);
		return -1;
	}

	if (chmod (sockpath, mode)) {
		perror ("chmod");
		close (sock);
		return -1;
	}

	if (listen (sock, max_conn) < 0) {
		perror ("listen");
		close (sock);
		return -1;
	}

	return sock;
}

/* code derived from:
 * http://mischasan.wordpress.com/2011/05/25/redirecting-stderr-to-syslog/
 */

static int noop (void) {
	return 0;
}

static size_t log_writer (void *cookie, char const *data, size_t len) {
	(void) cookie;

	syslog (LOG_ERR, "%.*s", (int) len, data);

	return len;
}

void tolog (FILE **pfp) {
	*pfp = fopencookie (NULL, "w", (cookie_io_functions_t) {
		(void*) noop,
		(void*) log_writer,
		(void*) noop,
		(void*) noop
	});
    setvbuf (*pfp, NULL, _IOLBF, 0);
}

void fHexDump (FILE *f, const void *buf, int len) {
	char ob [100];
	char ascii [20];
	int i;
	int n;
	char *p;
	const unsigned char *b;

	ascii [16] = 0;
	p = ob;
	b = buf;
	n = 15;
	for (i = 0; i < len; i++) {
		n = i % 16;
		if (n == 0) p += sprintf (p, "%04x  ", i);
		ascii [n] = isgraph (*b) ? *b : '.';
		p += sprintf (p, "%02x ", *b++);
		if (n == 15) {
			fprintf (f, "%s %s\n", ob, ascii);
			p = ob;
		}
	}
	if (n < 15) {
		while (n < 15) {
			ascii [n++] = ' ';
			p += sprintf (p, "   ");
		}
		fprintf (f, "%s %s\n", ob, ascii);
	}
} /* fHexDump () */

