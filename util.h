/*
 * util.h - header for util.c
 *
 * by Robert Edwards <bob@cs.anu.edu.au>, June 2014
 */

#ifndef __UTIL_H
#define __UTIL_H

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <syslog.h>

#ifndef DEBUG_LEVEL
#ifdef DEBUG
#define DEBUG_LEVEL LOG_DEBUG
#else
#define DEBUG_LEVEL LOG_ERR
#endif
#endif

extern int dbglvl;

#ifndef debug_msg
/*
 * adapted from:
 * http://stackoverflow.com/questions/1644868/c-define-macro-for-debug-printing
 * compiler will optimise away if lvl > DEBUG_LEVEL, and
 * runtime messages suppressed if lvl > dbglvl
 * LOG_EMERG normally won't be suppressed and will result in program termination
 */
#define debug_msg(lvl, fmt, ...) \
	do { \
		if ((lvl <= DEBUG_LEVEL) && (lvl <= dbglvl)) { \
			char *errstr = (lvl <= LOG_ERR) ? strerror (errno) : ""; \
			fprintf (stderr, "%s:%d:%s(): " fmt " %s\n", __FILE__, __LINE__, \
				__func__, ##__VA_ARGS__, errstr); \
		} \
		if (lvl == LOG_EMERG) exit (EXIT_FAILURE); \
	} while (0)
#endif

int timed_read (int sock, char *buf, int len, int secs);
int try_write (int sock, char *buf, int len);
struct sockaddr *get_sockaddr (struct sockaddr *addr, const char *canon);
int tcp_connect (struct sockaddr *addr, int port);
int tcp_listen (int family, int port);
int uds_connect (const char *sockpath);
int uds_listen (const char *sockpath, mode_t mode, int max_conn);
void tolog (FILE **pfp);
void fHexDump (FILE *f, const void *buf, int len);

#endif /* __UTIL_H */
