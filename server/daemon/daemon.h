/*
 *
*/

#ifndef __DAEMON_H_
#define __DAEMON_H_

#define DEBUG

#include "../../util.h"

#define VERSION "0.4"

/*
 * The port the damon listens on for incomming connections
 */
#define PORT 8787

/*
 * The maximum length of a username or password
 */
#define MAX_USER 15
#define MAX_PASS 63
/*
 * The character username and password are expected to be separated by on
 * the wire.
 */
#define SEP_CHAR '\t'

/*
 * Since each time a user logs into a machine a connection to the server
 * daemon is made (i.e. for each login from a VT), we must limit the number
 * of connections overall, and per IP, so that a DoS attach cannot be launched
 */
#define MAX_CONNECTIONS 255
#define MAX_CONNECTIONS_PER_IP 5

/*
 * The maximum time )in seconds that we wait for new heartbeats (which should 
 * arive each second) before disconnecting.
 */
#define MAX_LOST_HEARTBEAT_TIME 10

/*
 * Proc files for manipulating the kernel table.
 * Typically /proc/nfs_filter/table_add and /proc/nfs_filter/table_del
 */
#define FILTER_TABLE_DIR "/proc/net/nfs_filter/"
#define FILTER_TABLE_ADD_FILE "ip_add"
//#define FILTER_TABLE_DEL_FILE "/proc/nfs_filter/table_del"
//#define FILTER_TABLE_ADD_FILE "add_file.txt"
//#define FILTER_TABLE_DEL_FILE "del_file.txt"

/*
 * The message presented to the user when they connect to the daemon
 */
#define LOGIN_MESSAGE "NFS Filter server ver "VERSION" Apr 2013\n"

/*
 * Where to find the script files
 */
#define SCRIPT_BASE             "/etc/nfs_filtd"
#define NEW_CONNECTION_SCRIPT   SCRIPT_BASE"/new_connection.sh"
#define BREAK_CONNECTION_SCRIPT SCRIPT_BASE"/break_connection.sh"
#define START_SCRIPT            SCRIPT_BASE"/start_script.sh"
#define DIE_SCRIPT              SCRIPT_BASE"/die_script.sh"

#define CERTIFICATE_FILE  SCRIPT_BASE"/server.pem"
#define DEFAULT_CONFIG_FILE SCRIPT_BASE"/nfs_filtd.conf"

#endif
