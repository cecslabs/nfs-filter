#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h> 
#include <netinet/in.h>
#include <arpa/inet.h>

#include <openssl/ssl.h>
#include <openssl/err.h>

#include "daemon.h"

#ifndef MAX_CONNECTIONS
#define MAX_CONNECTIONS 255
#endif

#define MAX_LINE 1024

//#define CERT_FILE "/usr/share/nfs_filtd/server.pem"

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

typedef unsigned long int u32;

struct connection {
	struct in_addr addr;	/* address of connected party              */
	uid_t uid;
	int valid;
	int filter_built;
	time_t time;
};

void conn_table_init ();
int conn_table_count ();
int conn_table_add_connection (struct in_addr addr);
int conn_table_add_user (int pos, uid_t uid);
void conn_table_remove (int pos);
int conn_table_build_filter (struct in_addr addr, int pos, uid_t uid, gid_t *gid);
void conn_table_destroy_filter (int pos);
void conn_table_log (int pos, char *username, int in);
int conn_table_get (int pos, struct in_addr *s_addr, uid_t *uid, time_t *time);
