#!/bin/bash

NFS_FILTER_TABLE_DELETE="/proc/nfs_filter/table_del"

echo "*" > $NFS_FILTER_TABLE_DELETE

# Hugh 2013 Don't want to shut down NAT
#/sbin/iptables -F -t nat
#echo "0" >/proc/sys/net/ipv4/ip_forward
