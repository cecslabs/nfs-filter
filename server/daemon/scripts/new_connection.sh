#!/bin/bash

# This script runs on the router/server each time a new user authenticates.
# Not much that needs to be done here anymore - RHE, April 2013.
 
# IPTABLES=/sbin/iptables
# IFCONFIG=/sbin/ifconfig
# DEV=eth0

# find the IP address of the DEV interface
# ADDR=`$IFCONFIG $DEV | grep 'inet ' | awk '{print $2}'| cut -f2 -d":"`

#expect to be passed the ipaddress of the new connection as the first argument
# if [ -z $1 ]; then
#     echo "Need argument"
#     exit 1
# fi

# $IPTABLES -t nat -A POSTROUTING -o $DEV -j SNAT -s $1 --to $ADDR
# exit $?
exit 0
