#!/bin/sh
# This script is designed to allow external access to machines behind the NAT
# firewall on the provate sub net using DNAT

# Netfilter needs to be enableds as well as the multiport functions:
# CONFIG_IP_NF_MATCH_MULTIPORT=y 


# The ports that are allowed through
PORTS=www,ssh,https

# Where to find the lost of hosts to use allow through
# This file should have an entry on each line of the form:
#   <private net host>:<public net host>
# e.g. 192.168.1.200:150.203.24.201
HOSTS=/etc/nfs_filtd/dnat_hosts

# the utilities we use
IPTABLES=/sbin/iptables
IP=/sbin/ip

# flush the old rules
$IPTABLES -t nat -F PREROUTING

HSTS=`cat $HOSTS`
for h in $HSTS; do
    PRIV=` echo $h|cut --delimiter=':' -f1`
    EXTRN=`echo $h| cut --delimiter=':' -f2`

    $IP address add $EXTRN/24 dev eth0
    $IPTABLES -t nat -A PREROUTING -p tcp -m multiport --dport $PORTS -d $EXTRN -j DNAT --to $PRIV
    $IPTABLES -t nat -A POSTROUTING -d $PRIV -p tcp -m multiport --dport $PORTS -j ACCEPT 
done


