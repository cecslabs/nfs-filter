/*
 * daemon.c
 *
 * Part of nfs_filter by Matthew Pratt <mattpratt@yahoo.com> 15/12/2000
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>
#include <errno.h>
#include <stdarg.h>
#include <signal.h>
#include <syslog.h>
#include <pwd.h> 
#include <sys/stat.h>
#include <sys/wait.h>

#include "daemon.h"
#include "connection_table.h"
#include "config.h"

int dbglvl = LOG_ERR;
char *program_name;

void process_connection (int fd, struct in_addr addr);

void sig_chld (int sig)
{
	while (1) {
		if (wait (NULL) == -1) {
			if (errno != EINTR)
				return;
		} else return;
	}
} /* sig_chld () */

void report_status (int fd)
{
	char message[1024];
	int i;
	struct in_addr s_addr;
	uid_t uid;
	time_t time;

	strncpy (message, "nfs_filtd: current connections:\n", sizeof(message));
	if (write (fd, message, strlen (message) + 1) < 0);
	for (i = 0; i < MAX_CONNECTIONS; i++) {
		if (conn_table_get (i, &s_addr, &uid, &time) >= 0) {
			snprintf (message, sizeof(message), "%s %u %s",
					inet_ntoa (s_addr), uid, ctime (&time));
			if (write (fd, message, strlen (message) + 1) < 0);
		}
	}
	close (fd);
	exit (EXIT_SUCCESS);
} /* report_status () */

/*
 * The main event loop for the daemon. 
 */
void main_loop (int listen_sock) {
	int fd;
	struct sockaddr_in addr;
	socklen_t addrlen = sizeof(struct sockaddr_in);

	signal (SIGCHLD, sig_chld);

	while (1) {
		fd = accept (listen_sock, (struct sockaddr *)&addr, &addrlen);
		if (fd == -1) continue;
		if (conn_table_count () >= MAX_CONNECTIONS - 1) {
			usleep (100000);
		} else {
			if (fork () == 0) { /* child */
				close (listen_sock);
				if (addr.sin_addr.s_addr != htonl (INADDR_LOOPBACK)) {
					process_connection (fd, addr.sin_addr);
				} else {
					report_status (fd);
				}
				_exit(0);
			}
		}
		close (fd);
	}
} /* main_loop () */

void die_signal_handler (int sig) {
	debug_msg (LOG_NOTICE, "Running death script");
	if (system (DIE_SCRIPT) < 0);
	exit (1);
}

void usage(char * program , char * message ) {
    fprintf(stderr,"%s\n" , message );
    fprintf(stderr,"usage : %s [-c <config-file>] [-d] [-h] [-P]\n", program );
    fprintf(stderr,"\t-c <config-file>\tread configuration from <config-file>\n");
    fprintf(stderr,"\t-d \t\trun in debug (=non-daemon) mode.\n");
    fprintf(stderr,"\t-P \t\tprint configuration on stdout and exit.\n");
    fprintf(stderr,"\t-h \t\tthis message.\n");
} 

int get_options( int argc, char ** argv ) {
	char c = 0;
	int not_daemon = 0;
	int want_printout = 0;
	char * progname = argv[0];

	config_load( DEFAULT_CONFIG_FILE );

	while( (c = getopt( argc, argv, "c:dhP")) != EOF ) {
		switch(c) {
			case 'c':
				config_load(optarg);
				break;
			case 'd':
				not_daemon = 1;
				dbglvl = LOG_DEBUG;
				break;
			case 'h':
				usage(progname,"");
				return -1;
			case 'P':
				want_printout = 1;
				break;
			default:
				usage(progname,"");
				return -1;
		}
    }
 
    /** unset daemon-mode if -d was given. */
    if( not_daemon ) {
        config.daemon_mode = 0;
    }
 
    if( want_printout ) {
        config_print();
        exit(0);
    }  
    return 0;
}

int main (int argc, char **argv)
{                                 
	int listen_sock;

	program_name = rindex (argv[0], '/');
	program_name = program_name ? program_name + 1 : argv[0];

	/* get commandline options, load config if needed. */
	if (get_options (argc, argv) < 0) return EXIT_FAILURE;

	signal (SIGPIPE, SIG_IGN);
	listen_sock = tcp_listen (AF_INET, PORT);
	if (listen_sock < 0) return EXIT_FAILURE;
	conn_table_init ();

	if (system (START_SCRIPT) < 0);
   
	if (config.daemon_mode) {
		/* 
		 * catch all these signals to make sure the nfs filter table and the 
		 * IP NAT table gets cleared.
		 */
		signal(SIGINT,  die_signal_handler);
		signal(SIGTERM, die_signal_handler);
		signal(SIGQUIT, die_signal_handler);
		signal(SIGKILL, die_signal_handler);
		signal(SIGABRT, die_signal_handler);
		signal(SIGSEGV, die_signal_handler);
		signal(SIGTERM, die_signal_handler);
	
		if (daemon (0, 0) == -1) {
			exit (EXIT_FAILURE);
		}
		openlog (program_name, LOG_PID, LOG_LOCAL6);
		tolog (&stderr);
	}

	main_loop (listen_sock);
	return EXIT_SUCCESS;
} /* main () */
