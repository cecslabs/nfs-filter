#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h> 
#include <grp.h>
#include <stdio.h>
#include <stdarg.h> 
#include <string.h>   
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <lber.h>
#include <ldap.h>
#include <syslog.h>
#include <pwd.h>
#include <signal.h>

#include <openssl/ssl.h>
#include <openssl/err.h>

#include "daemon.h"
#include "connection_table.h"
#include "config.h"

/*
 * Initialise the SSL setup, and start the SSL connection over fd
 */
SSL *ssl_init (int fd) {
	SSL_CTX *ctx;
	SSL *ssl;

	SSL_load_error_strings ();
	SSLeay_add_ssl_algorithms ();
	ctx = SSL_CTX_new (SSLv23_server_method ());
	if (ctx &&
		SSL_CTX_use_certificate_file (ctx, CERTIFICATE_FILE,
			SSL_FILETYPE_PEM) &&
		SSL_CTX_use_PrivateKey_file (ctx, CERTIFICATE_FILE, 
			SSL_FILETYPE_PEM) &&
		SSL_CTX_check_private_key (ctx)) {
		ssl = SSL_new (ctx);
		if (ssl) {
			SSL_set_fd (ssl, fd);
			if (SSL_accept (ssl) > 0)
				return ssl;
		}
	}
	if ((LOG_ERR <= DEBUG_LEVEL) && (LOG_ERR <= dbglvl)) {
		ERR_print_errors_fp (stderr);
	}
	return NULL;
} /* ssl_init () */

/*
 * Authenticate the user "user" with password "pass" against the LDAP server
 * "server" with LDAP base "base".
 * RETURN:
 *     o -1 on authentication or other failure
 *     o 0 on success
 */
int authenticate_ldap (char *user, char *pass, char *uri, char *base) {
	LDAP *ld;
	int ldap_version = 3;
	struct berval cred;
	char who[1024]; 
	int retval;

	if (!user || !pass || !user[0] || !pass[0])
		return -1;

//	if (NULL == (ld = ldap_init (server, config.ldap_port))) {     
	retval = ldap_initialize (&ld, uri);
	if (retval != LDAP_SUCCESS) {
		syslog (LOG_ERR, "Could not initialize connection to LDAP uri '%s'", uri);
		return -1;
	}
	
	retval = ldap_set_option (ld, LDAP_OPT_PROTOCOL_VERSION, &ldap_version);
	if (LDAP_SUCCESS != retval) {
	    debug_msg (LOG_NOTICE,
			"nfs_filtd: ldap_set_option failed: %s\n", ldap_err2string (retval));
	}
	
	snprintf (who, sizeof(who), "uid=%s,%s", user, base);
	cred.bv_val = (char *) pass;
        cred.bv_len = strlen (pass);
	retval = ldap_sasl_bind_s (ld, who, LDAP_SASL_SIMPLE, &cred, NULL, NULL, NULL);
	if (retval != LDAP_SUCCESS) {
		debug_msg (LOG_NOTICE,
			"nfs_filtd: ldap bind to '%s' as '%s' failed: %s\n",
			uri, who, ldap_err2string (retval));
	}
	ldap_unbind_ext_s (ld, NULL, NULL);
	return retval;
} /* authenticate_ldap () */

/*
 * Authenticate the user "user" with password "pass" agaist both LDAP servers
 * specified in the config file (ldap_server, and aux_ldap_server).
 * RETURN:
 *     o -1 on authentication or other failure
 *     o 0 on success
 */
int authenticate_user (char *user, char *pass)
{ 
	if (authenticate_ldap (user, pass, config.ldap_uri, 
			config.ldap_base) == LDAP_SUCCESS)
		return 0;
	if (authenticate_ldap (user, pass, config.aux_ldap_uri, 
			config.aux_ldap_base) == LDAP_SUCCESS)
		return 0;

	return -1;
} /* authenticate_user () */

/*
 * Perform a lookup of the users UID/GID.
 */
int get_uid_gid (char *user, uid_t *uid, gid_t *gid)
{
	struct passwd *pw;

	pw = getpwnam (user);
	if (pw == NULL) {
		debug_msg (LOG_DEBUG, "getpwnam for %s returned NULL\n", user);
		return -1;
	}

	if (pw->pw_uid == 0) return -1;

	*uid = pw->pw_uid;
	*gid = pw->pw_gid;
	return 0;
} /* get_uid_gid () */

/*
 * Print the given arguments down the SSL connection
 */
void print_ssl (SSL *ssl, char *fmt, ...)
{
	va_list args;
	char text[128];

	va_start (args, fmt);
	vsnprintf (text, sizeof(text), fmt, args);
	va_end (args);
	SSL_write (ssl, text, strlen(text));
} /* print_ssl () */

/*
 * Read a line from the SSL connection
 * RETURNS:
 *     o a pointer to a static buffer holding the line
 */
char *read_line (SSL *ssl)
{
	static char line[MAX_LINE];
	int pos, nread = 1;

	bzero (line, sizeof(line));

	pos = 1;
	while (nread > 0 && line[pos - 1] != '\n' && pos < sizeof(line)) {
		nread = SSL_read (ssl, &line[pos++], 1);
	}
	line[pos - 1] = 0;

	return &line[1];
} /* read_line () */

/*
 * Read the username and password from the given connection.
 * username and password will be modified to point to static buffers.
 * ASSUMES:
 *       o username and password are seperated by SEP_CHAR
 */
void get_user_pass (SSL *ssl, char **username, char **password)
{
	static char user[MAX_PASS + 1];
	static char pass[MAX_PASS + 1];
	char *line;
	int pass_start = -1;
	int i;

	*username = user;
	*password = pass;

	bzero (user, sizeof(user));
	bzero (pass, sizeof(pass));

	line = read_line (ssl);

	for (i = 0; line[i]; i++) {
		if (line[i] == '\r')
			line[i] = 0;
		if (line[i] == SEP_CHAR && pass_start == -1) {
			pass_start = i;
		}
	}

	if (pass_start != -1 ) {
		line[pass_start] = 0;
		strncpy (pass, &line[pass_start+1], MAX_PASS);
	}
	strncpy(user, line, MAX_PASS);
} /* get_user_pass () */

static int getauxgrps (char *user, gid_t gids[], int num)
{
	struct group *grp;
	char **memb;
	int count = 0;

	setgrent ();
	while (((grp = getgrent ()) != 0) && (count < num)) {
		for (memb = grp->gr_mem; *memb; ++memb) {
			if (strcmp (user, *memb) == 0) {
				*gids++ = grp->gr_gid;
				count++;
				break;
			}
		}
	}
	endgrent ();
	*gids = -1;
	return count;
} /* getauxgrps () */

/*
 * Wait for and receive regular heart beats from the client. If there is a 
 * period of no heartbeats lognger than  MAX_LOST_HEARTBEAT_TIME then we
 * disconnect the user.
 */
void monitor_heartbeat (int fd, SSL *ssl, struct in_addr addr) {
	struct timeval tv;
	fd_set rfds;
	char buff[MAX_LINE];
	time_t time_stamp;

	time_stamp = time (NULL);
	while (1) {
		/* set the one second time out */
		tv.tv_sec = MAX_LOST_HEARTBEAT_TIME;
		tv.tv_usec = 0;
		/* setup the set and include the listen socket */
		FD_ZERO(&rfds);
		FD_SET(fd, &rfds);
	
		if (select (FD_SETSIZE, &rfds, NULL, NULL, &tv)) 
			if (FD_ISSET(fd, &rfds)) {
				if (SSL_read (ssl, buff, sizeof(buff)) <= 0) {
					debug_msg (LOG_INFO, "Client disconnect from %s", 
						inet_ntoa (addr));
						/* wait 2 secs for any pending F/S ops to complete */
						sleep (2);
					return;
				}
				time_stamp = time(NULL);
			}
	
		if (time (NULL) - time_stamp > MAX_LOST_HEARTBEAT_TIME) {
			debug_msg (LOG_NOTICE, "Heartbeat lost for some time from %s", 
					inet_ntoa (addr));
			return;
		}
	}
} /* monitor_heartbeat () */

int add_filter (int pos, struct in_addr addr, char *user)
{
	uid_t uid;
	gid_t gids[34];

	if (get_uid_gid (user, &uid, &gids[0]) == 0) {
//		if (getauxgrps (user, &gids[1], 32) == 0) {
		getauxgrps (user, &gids[1], 32);
		if (conn_table_add_user (pos, uid) == 0) {
			if (conn_table_build_filter (addr, pos, uid,
					gids) == 0) {
				return 0;
			} else {
				debug_msg (LOG_WARNING,
					"Filter build failed for %s: %s, %d, %d",
					user, inet_ntoa (addr), pos, uid);
			}
		} else {
			debug_msg (LOG_NOTICE, "Another user on that IP");
		}
//		} else {
//			debug ("Unable to get auxiliary groups\n");
//		}
	} else {
		debug_msg (LOG_NOTICE, "UID/GID lookup failure");
	}
	return -1;
} /* add_filter () */

/*
 * The main procedure for the servlet. fd should be connected to the client
 */
void process_connection (int fd, struct in_addr addr)
{
	int pos;
	char *user, *pass;
	SSL *ssl;

	signal (SIGINT,  SIG_DFL);
	signal (SIGTERM, SIG_DFL);
	signal (SIGQUIT, SIG_DFL);
	signal (SIGKILL, SIG_DFL);
	signal (SIGABRT, SIG_DFL);
	signal (SIGSEGV, SIG_DFL);
	signal (SIGTERM, SIG_DFL);
    
	pos = conn_table_add_connection (addr);
	if (pos >= 0) {
		ssl = ssl_init (fd);
		if (ssl) {
			get_user_pass (ssl, &user, &pass);
			debug_msg (LOG_DEBUG, "User '%s' connected from %s",
				user, inet_ntoa (addr));

			if (authenticate_user (user, pass) == 0) {
				if (add_filter (pos, addr, user) == 0) {
					print_ssl (ssl, "SUCCESS\n");
					debug_msg (LOG_DEBUG, "Yay...success");
					conn_table_log (pos, user, TRUE);

					/* heartbeat stuff */
					monitor_heartbeat (fd, ssl, addr);
					conn_table_log (pos, user, FALSE);
				}
			} else {
				print_ssl (ssl, "AUTHENTICATION FAILED\n");
				debug_msg (LOG_DEBUG, "Auth failure");
			}
			SSL_shutdown (ssl);
			SSL_free (ssl);
		}
	} else {
		debug_msg (LOG_WARNING, "Connection setup failure");
	}
	close(fd);
	conn_table_destroy_filter (pos);
	_exit(1);
} /* process_connection () */
