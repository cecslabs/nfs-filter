#include <sys/mman.h>
#include <string.h> 
#include <sys/ipc.h>
#include <sys/sem.h> 
#include <syslog.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "connection_table.h"
#include "daemon.h"

/* globals */
struct connection *conn_table;
int sem_id;

void conn_table_init () {
	conn_table = mmap (0, MAX_CONNECTIONS * sizeof(struct connection),
			PROT_READ | PROT_WRITE,  MAP_SHARED | MAP_ANON, 0, 0);
	if (conn_table == MAP_FAILED) {
		perror ("mmap");
		exit (1);
	}

	bzero (conn_table, MAX_CONNECTIONS * sizeof(struct connection));
    
	/* create a private semaphore set with one semaphore in it, */
	/* with access only to the owner.                           */
	sem_id = semget (IPC_PRIVATE, 1, IPC_CREAT | 0600);
	if (sem_id == -1) {
		perror ("semget");
		exit (1);
	}

	/* initialize the first semaphore in our set to '1'.  */
	if (semctl (sem_id, 0, SETVAL, 1) == -1) {
		perror ("semctl");
		exit (1);
	}
} /* conn_table_init () */

void conn_table_lock()
{
    struct sembuf sem_op;

    /* wait on the semaphore, unless it's value is non-negative. */
    sem_op.sem_num = 0;
    sem_op.sem_op = -1;   /* <-- Comment 1 */
    sem_op.sem_flg = 0;
    semop (sem_id, &sem_op, 1);
}

void conn_table_unlock()
{
    struct sembuf sem_op;

    /* finally, signal the semaphore - increase its value by one. */
    sem_op.sem_num = 0;
    sem_op.sem_op = 1;   /* <-- Comment 3 */
    sem_op.sem_flg = 0;
    semop(sem_id, &sem_op, 1);
}

int conn_table_count()
{
    int i, count = 0;
    conn_table_lock();
    for( i = 0; i < MAX_CONNECTIONS; i++){
	if(conn_table[i].valid == TRUE)
	    count++;
    }
    conn_table_unlock();
    return count;
}

/*****************************************************************************/

/* These functions dont lock the conn_table and so the caller is responsible
 * for doing this
 */
int conn_table_count_addr (struct in_addr addr) {
	int i;
	int count = 0;
	struct connection *conn = conn_table;

	for (i = 0; i < MAX_CONNECTIONS; i++) {
		if (conn->valid && (conn->addr.s_addr == addr.s_addr))
			count++;
		conn++;
	}
	return count;
} /* conn_table_count_addr () */

int conn_table_find_free ()
{
	int i;
	for (i = 0; i < MAX_CONNECTIONS; i++) {
		if (!conn_table[i].valid)
			return i;
	}
	return -1;
} /* conn_table_find_free () */

int conn_table_filter_already_built (struct in_addr addr)
{
	int i;
	struct connection *conn = conn_table;

	for (i = 0; i < MAX_CONNECTIONS; i++, conn++) {
		if (conn->valid && (conn->addr.s_addr == addr.s_addr) &&
				(conn->uid != -1) && (conn->filter_built))
		       	return 1;
	}
	return 0;
} /* conn_table_filter_already_built () */

/*****************************************************************************/

int conn_table_add_connection (struct in_addr addr)
{
	int pos = -1;

	conn_table_lock ();
	if (conn_table_count_addr (addr) < MAX_CONNECTIONS_PER_IP) {
		pos = conn_table_find_free ();
		if (pos >= 0) {
			struct connection *conn = &conn_table[pos];
			conn->addr = addr;
			conn->valid = 1;
			conn->uid = -1;
			conn->filter_built = 0;
			conn->time = time (NULL);
		}
	}
	conn_table_unlock ();
	return pos;
} /* conn_table_add_connection () */

int conn_table_add_user (int pos, uid_t uid)
{
	int i;
	int retval = -1;
	u32 addr;
	struct connection *conn = conn_table;

	conn_table_lock ();
	addr = conn_table[pos].addr.s_addr;

	for (i = 0; i < MAX_CONNECTIONS; i++) {
		if (conn->valid != TRUE) continue; 
		if (conn->addr.s_addr != addr) continue;
		if (conn->uid == -1) continue;
		if (conn->uid != uid)
			goto done;
	}
	conn_table[pos].uid = uid;
	retval = 0;
	done:
	conn_table_unlock();
	return retval;
}

void conn_table_remove (int pos)
{
	conn_table_lock();
	conn_table[pos].valid = FALSE;
	conn_table[pos].uid = -1;
	conn_table[pos].filter_built = FALSE;
	conn_table_unlock();
}

/***************************************************************************** 
 * The following two filter routines tell the kernel which users should be
 * allowed to pass NFS packets through the system from what IP.
 * Typically this is done via a proc interface to the nf_nfs_filt module.
 * The /proc/nfs_filter/table_add is given a line with space separated
 * IP and UID of an allowed user. e.g. "192.168.0.5 502"
 * The /proc/nfs_filter/table_del is used to delete users when the disconnect
 * Only an ip need be written to this file.
 */

int conn_table_build_filter (struct in_addr addr, int pos, uid_t uid, gid_t *gid)
{
	char *ip;
	char command[1024];
	char ipfile[1024] = FILTER_TABLE_DIR;
	int result;
	FILE *fp = NULL;

	conn_table_lock ();
	if (conn_table_filter_already_built (addr)) {
		conn_table_unlock ();
		return 0; /* connection must already be built */
	}

	ip = inet_ntoa (addr);
	if (!ip) {
		syslog (LOG_ERR, "Invalid IP for uid %d", uid);
		goto failure;
	}

	command[0] = '\0';
	while ((*gid != -1) && (strlen (command) < 1024)) {
		char agid[10];
		snprintf (agid, 10, "%d ", *gid++);
		strncat (command, agid, 1023);
	}

	/* add to nfs_filter table */
	fp = fopen (FILTER_TABLE_DIR FILTER_TABLE_ADD_FILE, "w");
	if (!fp) {
		syslog (LOG_ERR, "Unable to open ip_add for uid %d", uid);
		goto failure; 
	}
	syslog (LOG_NOTICE, "Writing %s to ip_add", ip);
	fprintf (fp, "%s", ip);
	fclose (fp);

	strncat (ipfile, ip, 1023);
	fp = fopen (ipfile, "w");
	if (!fp) {
		syslog (LOG_ERR, "Unable to open %s for uid %d", ip, uid);
		goto failure; 
	}
	syslog (LOG_NOTICE, "Writing %d %s to %s", uid, command, ip);
	fprintf (fp, "%d %s", uid, command);
	fclose (fp);
	fp = NULL;

	/* now run any other commands (for building up iptables rules) */
	snprintf (command, sizeof (command), "%s %s %u %u",
					NEW_CONNECTION_SCRIPT, ip, uid, *gid);
	result = system (command);
	if (result != 0) {
		syslog (LOG_ERR, "error - system (%s) returned %d", command, result);
		goto failure;
	}

	syslog (LOG_NOTICE, "info - system (%s) returned %d", command, result);
	conn_table[pos].filter_built = TRUE;
	conn_table_unlock ();
	return 0;

failure:
	conn_table[pos].uid = -1;
	conn_table_unlock ();
	if (fp) fclose (fp);
	return -1;
} /* conn_table_build_filter () */

void conn_table_destroy_filter (int pos)
{
	char *ip;
	char command[1024];
	char ipfile[1024] = FILTER_TABLE_DIR;
	FILE *fp;
	int retval;
	
	conn_table_lock();

//    if(conn_table_count_addr(conn_table[pos].addr) != 0){
//	conn_table_unlock();
//	return;
//    }

	ip = inet_ntoa (conn_table[pos].addr);
    
	/* remove from filter table */
	strncat (ipfile, ip, 1023);
	fp = fopen (ipfile, "w");
	if (fp) {
		fprintf (fp, "%d - \n", conn_table[pos].uid);
		fclose (fp);   
	}

	/* run any other scripts (maybe for iptable rules) */
	snprintf (command, sizeof(command), "%s %s %d", BREAK_CONNECTION_SCRIPT, 
			ip, conn_table[pos].uid);
	retval = system (command);
	if (retval == -1) {
		syslog (LOG_NOTICE, "info - system (%s) returned %d", command, retval);
	}

	conn_table[pos].valid = FALSE;
	conn_table[pos].uid = -1;
	conn_table[pos].filter_built = FALSE;

	conn_table_unlock ();
}

void conn_table_log (int pos, char *username, int in)
{
	struct in_addr addr = conn_table[pos].addr;

	if (in) {
		if (conn_table_count_addr (addr) == 1)
			syslog (LOG_NOTICE, "User %s login from %s", username,
					inet_ntoa (addr));
	} else {
		if (conn_table_count_addr (addr) == 1)
			if (conn_table[pos].uid != -1)
				syslog (LOG_NOTICE, "User %s disconnect", username);
	}
}

int conn_table_get (int pos, struct in_addr *s_addr, uid_t *uid, time_t *time)
{
	struct connection *conn;

	if ((pos < 0) || (pos >= MAX_CONNECTIONS))
		return -1;

	conn = &conn_table[pos];
	if (!conn->valid || !conn->filter_built)
		return -1;

	*s_addr = conn->addr;
	*uid = conn->uid;
	*time = conn->time;
	return 0;
} /* conn_table_get () */
