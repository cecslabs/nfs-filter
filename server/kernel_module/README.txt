nfsfilt kernel module

This module can be built outside the kernel tree using make:

% make

It should compile cleanly and leave a file: xt_NFSFilt.ko in this
directory, which can be loaded into the kernel using insmod.

See: https://www.kernel.org/doc/Documentation/kbuild/modules.txt
for the full details of building kernel modules "out-of-tree".
