/*
 * xt_NFSFilt - filter NFS requests by RPC uid, gid and source IP
 *
 * This module normally sits in the iptables FORWARD "mangle" table of a
 * trusted SNAT router and checks AUTH_UNIX credentials of NFS requests
 * against kernel-maintained tables of source IP address, uid, gid and
 * auxiliary gids, mangling "illegal" requests such that they hit the NFS
 * server as a benign user.
 *
 * Module is intended to be used with a "large" (> 10) number of NFS
 * clients, but a small (< 5) number of concurrent users per client
 * (typically 1) and, in the case of NFSv3 over TCP, a small (< 5) number
 * of target servers.
 *
 * compile outside the kernel tree with:
 *  make
 *
 * When loaded, this module will create a directory: /proc/net/nfs_filter,
 * with files:
 *  ip_add - write-only file to add a new IP
 *  status - read-only status of the module
 *  <ipaddr> - read/write files for each IP address registered
 *
 * To use, first the iptables command will need to be re-compiled to add the
 * NFSFILT target, and then add an entry or two into the "mangle" table:
 *  iptables -t mangle -A FORWARD -s 192.168.0.0/16 -p udp --dport 2049 -j NFSFILT
 *  iptables -t mangle -A FORWARD -s 192.168.0.0/16 -p tcp --dport 2049 -j NFSFILT
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Copyright 2000 Matthew Pratt
 * Copyright 2001 - 2014 Robert (Bob) Edwards (bob@cs.anu.edu.au)
 *
 * (with help from Eric McCreath and Steven Hanley)
 *
 * 2013-01-18: builds against linux kernel 3.2.0
 * 2013-04-18: tested against 64-bit kernel
 * 2014-08-14: moved to new(er) /proc interface
 * 2014-08-19: name change, adding TCP and NFSv3
 * 2015-05-12: more /proc interface tidying up for 3.13 kernel
 * 2016-05-20: fix significant errors with non-linear sk_buffs
 */

#include <linux/inet.h>
#include <linux/ip.h>
#include <linux/module.h>
#include <linux/netfilter_ipv4/ip_tables.h>
#include <linux/netfilter/x_tables.h>
#include <linux/nfs.h>
#include <linux/proc_fs.h>
#include <linux/skbuff.h>
#include <linux/sunrpc/msg_prot.h>
#include <linux/tcp.h>
#include <linux/udp.h>
#include <linux/uaccess.h>
#include <linux/version.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Matthew Pratt, Bob Edwards <bob@cs.anu.edu.au>");
MODULE_DESCRIPTION("filter NFS packets based on src addr, RPC uid and gid");

#define NFSFILT_VERS "3.3a w/TCP"
#define ROOT_SQUASH		/* change to NO_ROOT_SQUASH to not squash root */
#define MAX_TCP_SERVERS 5

#define DEBUG

#ifdef DEBUG
// #define DEBUGP(lvl, format, args...) do{if((dbglvl>=lvl) && net_ratelimit())
#define DEBUGP(lvl, format, args...) do{if((dbglvl>=lvl)) \
	printk("%s:" format, __FUNCTION__ , ## args);}while(0)
#else
#define DEBUGP(lvl, format, args...)
#endif

#ifndef NFS_NGROUPS
#define NFS_NGROUPS     16
#endif

#define MAX_HASH 16
#define MAX_UIDS 2

#define DEFAULT_SAFE_GID 65534
#define DEFAULT_SAFE_UID 65534
static int safe_uid = DEFAULT_SAFE_UID;
static int safe_gid = DEFAULT_SAFE_GID;
module_param (safe_uid, int, 0400);
module_param (safe_gid, int, 0400);
MODULE_PARM_DESC(safe_uid, "the uid to substitute");
MODULE_PARM_DESC(safe_gid, "the gid to substitute");

#ifdef DEBUG
static int dbglvl = 0;
module_param (dbglvl, int, 0600);
MODULE_PARM_DESC(dbglvl, "debugging level, defaults to 0");
#endif

#define CSUM_ADD16(x) (x & 0xffff) + (x >> 16)

/*
 * funnily enough, the kernel headers don't have a def. for an RPC call header.
 * from rfc1057:
 */
struct rpc_call_hdr {
	u32	xid;
	u32	msg_type;
	u32	rpc_vers;
	u32	prog;
	u32	vers;
	u32	proc;
	u32	flavor;
	u32 length;
};

#define MIN_UNIX_AUTH_WORDS 5	/* stamp, machnamelen, uid, gid, #aux */

/* per-authenticated user for a specific IP address */
struct nfsfilt_auth {
	int num_gids;		// == 0 => empty slot
	uid_t uid;			// stored in network byte order!
	gid_t gid [NFS_NGROUPS + 1];	// ditto
	int num_rpcs;
	int num_changed;
	unsigned long last_changed;	// jiffies of last change
};

/* poor-mans connection tracking for TCP RPC */
struct nfsfilt_tcprpc {
	u32 dest;			// dest IPv4 stored in network byte order!
	u32 nseq;			// seq no. of next RPC fragment header
	u16 dport;
	u16 sport;
};

/* per-IP address */
struct nfsfilt_addr {
	struct nfsfilt_addr *next;
	u32 addr;			// stored in network byte order!
	struct nfsfilt_auth auth[MAX_UIDS];
	struct proc_dir_entry *proc_entry;
// statistics
	int bad_proto;
	int bad_rpcsize;
	int bad_nfs;
	int not_auth_unix;
	int bad_namelen;
	int bad_ngids;
	int num_root;
	int bad_uid;
	int num_auth;
	int tcp_handshake;
	int bad_frags;
	int bad_remain;
	int tcp_frags;
	int bad_nserv;
	struct nfsfilt_tcprpc servs[MAX_TCP_SERVERS];
};

struct nfsfilt_xt_info {
	__u32 flags;
}; /* struct nfsfilt_xt_info */

/* Globals */
static struct nfsfilt_addr *nfsfilt_hashtable [MAX_HASH];
static int nfsfilt_nbad_addr = 0;
static struct proc_dir_entry *nfsfilt_proc_dir;
static struct proc_dir_entry *nfsfilt_proc_ip_add;
static struct proc_dir_entry *nfsfilt_proc_status;
static const char *nfsfilt_proc_dir_name = "nfs_filter";
static const char *nfsfilt_proc_ip_add_name = "ip_add";
static const char *nfsfilt_proc_status_name = "status";


static inline int nfsfilt_hash (const u32 addr)
{
	return (((addr >> 8) + addr) % MAX_HASH);
} /* nfsfilt_hash () */

static void nfsfilt_mem_free (void)
{
	int i;
	struct nfsfilt_addr *paddr, *paddr_next;

	for (i = 0; i < MAX_HASH; i++) {
		if (nfsfilt_hashtable[i]) {
			paddr = nfsfilt_hashtable[i];
			while (paddr) {
				paddr_next = paddr->next;
				kfree (paddr);
				paddr = paddr_next;
			}
		}
	}
} /* nfsfilt_mem_free () */

static inline char hex2ascii (unsigned char c) {
	c += '0';
	if (c > '9') c += 'a' - '9' - 1;
	return (char) c;
}

static void nfsfilt_dump (void *buf, int lines) {
	unsigned char *inp = buf;

	while (lines--) {
		char out[80];
		char *outp = out;
		int i;
		for (i = 0; i < (8 * 4); i++, inp++) {
			if ((i & 31) == 0) *outp++ = '\n';
			if ((i &  3) == 0) *outp++ = ' ';
			*outp++ = hex2ascii (*inp >> 4);
			*outp++ = hex2ascii (*inp & 15);
		}
		*outp = '\0';
		printk ("%s\n", out);
	}
} /* nfsfilt_dump () */

/*** nfsfilt proc interface ***********************************************/

static int nfsfilt_proc_ip_show (struct seq_file *m, void *v)
{
	struct nfsfilt_addr *addr = (struct nfsfilt_addr *) m->private;
	struct nfsfilt_auth *auth;

	seq_printf (m, "EE #unknown proto: %d, #bad rpc size: %d, #not nfs: %d\n",
		addr->bad_proto, addr->bad_rpcsize, addr->bad_nfs);
	seq_printf (m, "EE #bad namelen: %d, #bad ngids: %d, #bad uid: %d\n",
		addr->bad_namelen, addr->bad_ngids, addr->bad_uid);
	seq_printf (m, "II #not auth unix: %d, #uid 0: %d, #auths: %d\n",
		addr->not_auth_unix, addr->num_root, addr->num_auth);
	seq_printf (m, "II #tcp handshake: %d, EE #bad frags: %d\n",
		addr->tcp_handshake, addr->bad_frags);
	do {
		int i;
		seq_printf (m, "II TCP servers:");
		for (i = 0; i < MAX_TCP_SERVERS; i++) {
			if (addr->servs[i].dest) {
				seq_printf (m, " %pI4", &addr->servs[i].dest);
			}
		}
		seq_printf (m, "\n");
	} while (0);

	auth = &addr->auth[0];
	while (auth != &addr->auth[MAX_UIDS]) {
		if (auth->num_gids > 0) {
			int i;

			seq_printf (m,
				"II uid:%d pri. gid:%d #rpcs:%d #changed:%d jiffies:%lu\n",
				ntohl (auth->uid), ntohl (auth->gid[0]), auth->num_rpcs,
				auth->num_changed, jiffies - auth->last_changed);
			seq_printf (m, "II #gids:%u ", auth->num_gids);
			for (i = 1; i < auth->num_gids; i++) {
				seq_printf (m, "%d ", ntohl (auth->gid[i]));
			}
			seq_printf (m, "\n");
		}
		auth++;
	}
	return 0;
} /* nfsfilt_proc_ip_show () */

static int nfsfilt_proc_ip_open (struct inode *inode, struct file *file)
{
	return single_open (file, nfsfilt_proc_ip_show, PDE_DATA(inode));
} /* nfsfilt_proc_ip_open () */

static ssize_t nfsfilt_proc_ip_write (struct file *file,
		const char __user *ubuf, unsigned long count, loff_t *offset)
{
	struct nfsfilt_addr *addr = PDE_DATA(file_inode (file));
	struct nfsfilt_auth *auth;
	char buffer[128];
	char *strp;
	char *puid;
	char *pgid;
	uid_t uid;
	gid_t gid;

	if (count > sizeof (buffer)) {
		DEBUGP(0, "string too long\n");
		return -1;
	}

	if (copy_from_user (buffer, ubuf, count));
	buffer [count] = 0;
	strp = buffer;

	puid = strsep (&strp, " ");
	pgid = strsep (&strp, " ");
	if (!puid || !pgid) {
		DEBUGP(0, "invalid input\n");
		return -1;
	}

	uid = htonl (simple_strtol (puid, NULL, 0));

	if (*pgid != '-') {
		for (auth = &addr->auth[0]; auth != &addr->auth[MAX_UIDS]; auth++) {
			if ((auth->num_gids > 0) && (auth->uid == uid)) break;
		}
		if (auth == &addr->auth[MAX_UIDS]) {
			for (auth = &addr->auth[0]; auth != &addr->auth[MAX_UIDS]; auth++) {
				if (auth->num_gids == 0) break;
			}
			if (auth == &addr->auth[MAX_UIDS]) {
				DEBUGP(0, "MAX_UIDS exceeded for %pI4\n", &addr->addr);
				return -1;
			}
			memset (auth, 0, sizeof (struct nfsfilt_auth));
			auth->uid = uid;
			auth->last_changed = jiffies;
			addr->num_auth++;
		}

		/* update gids for new or existing entry */
		gid = simple_strtol (pgid, NULL, 10);
		auth->num_gids = 0;
		auth->gid[auth->num_gids++] = htonl (gid);
		while (strp && *strp && (auth->num_gids < NFS_NGROUPS)) {
			pgid = strsep (&strp, " ");
			if (pgid && *pgid &&
				(gid = simple_strtol (pgid, NULL, 10))) {
				auth->gid[auth->num_gids++] = htonl(gid);
			}
		}
	} else {
		/* remove uid from this client entry */
		int i;
		int num_uids = 0;
		for (i = 0; i < MAX_UIDS; i++) {
			if (addr->auth[i].uid == uid) {
				addr->auth[i].num_gids = 0;
			} else if (addr->auth[i].num_gids > 0) {
				num_uids++;
			}
		}
		if (num_uids == 0) {
			/* clear TCP server list */
			for (i = 0; i < MAX_TCP_SERVERS; i++) {
				addr->servs[i].dest = 0;
			}
		}
	}
	return count;
} /* nfsfilt_proc_ip_write () */

static const struct file_operations nfsfilt_proc_ip_ops = {
	.owner   = THIS_MODULE,
	.open    = nfsfilt_proc_ip_open,
	.read    = seq_read,
	.write   = nfsfilt_proc_ip_write,
    .llseek  = seq_lseek,
    .release = single_release,
};

static int nfsfilt_proc_status_show (struct seq_file *m, void *v)
{
	seq_printf (m, "NFSFilt version %s\n", NFSFILT_VERS);
	seq_printf (m, "dbglvl: %d, using safe uid: %d, safe gid: %d\n", dbglvl, safe_uid, safe_gid);
	seq_printf (m, "# bad IPs: %d\n", nfsfilt_nbad_addr);
	return 0;
} /* nfsfilt_proc_status_show () */

static int nfsfilt_proc_status_open (struct inode *inode, struct file *file)
{
	return single_open (file, nfsfilt_proc_status_show, NULL);
} /* nfsfilt_proc_status_open () */

static const struct file_operations nfsfilt_proc_status_ops = {
	.owner   = THIS_MODULE,
	.open    = nfsfilt_proc_status_open,
	.read    = seq_read,
    .llseek  = seq_lseek,
    .release = single_release,
};

static int nfsfilt_proc_ip_add_show (struct seq_file *m, void *v)
{
	seq_printf (m,
		"write ipv4addr (w.x.y.z) to this file to create filter for ip\n");
	return 0;
} /* nfsfilt_proc_ip_add_show () */

static ssize_t nfsfilt_proc_ip_add_write (struct file *file,
		const char __user *ubuf, unsigned long count, loff_t *offset)
{
	char temp[60];
	char *strp;
	char *pip;
	struct nfsfilt_addr **ppaddr;
	u32 addr;

	if (count >= sizeof (temp)) {
		DEBUGP(0, "string too long\n");
		return -1;
	}

	if (copy_from_user (temp, ubuf, count));
	temp[count] = 0;
	strp = temp;
	pip = strsep (&strp, " \n");

	addr = in_aton (pip);
	if (!addr) {
		DEBUGP(0, "bad IPv4 addr: %s\n", pip);
		return -1;
	}

	/* lookup IP address */
	ppaddr = &nfsfilt_hashtable [nfsfilt_hash (ntohl (addr))];
	while (*ppaddr && ((*ppaddr)->addr != addr)) ppaddr = &((*ppaddr)->next);
	if (*ppaddr == NULL) {
		*ppaddr = kmalloc (sizeof (struct nfsfilt_addr), GFP_ATOMIC);
		if (*ppaddr == NULL) {
			DEBUGP(0, "kmalloc failed\n");
			return -1;
		}
		memset (*ppaddr, 0, sizeof (struct nfsfilt_addr));
		(*ppaddr)->addr = addr;
		(*ppaddr)->proc_entry = proc_create_data (pip, 0600,
			nfsfilt_proc_dir, &nfsfilt_proc_ip_ops, *ppaddr);
	}
	return count;
} /* nfsfilt_proc_ip_add_write () */

static int nfsfilt_proc_ip_add_open (struct inode *inode, struct file *file)
{
	return single_open (file, nfsfilt_proc_ip_add_show, NULL);
} /* nfsfilt_proc_status_open () */

static const struct file_operations nfsfilt_proc_ip_add_ops = {
	.owner   = THIS_MODULE,
	.open    = nfsfilt_proc_ip_add_open,
	.read    = seq_read,
	.write   = nfsfilt_proc_ip_add_write,
    .llseek  = seq_lseek,
    .release = single_release,
};

void nfsfilt_proc_cleanup (void)
{
	if (nfsfilt_proc_dir) {
		proc_remove (nfsfilt_proc_dir);
		nfsfilt_proc_dir = NULL;
	}
} /* nfsfilt_proc_cleanup () */

static int __init nfsfilt_proc_init (void)
{
	nfsfilt_proc_dir = proc_mkdir (nfsfilt_proc_dir_name, init_net.proc_net);
	if (nfsfilt_proc_dir) {
		nfsfilt_proc_status = proc_create_data (nfsfilt_proc_status_name, 0400,
				nfsfilt_proc_dir, &nfsfilt_proc_status_ops, NULL);
		nfsfilt_proc_ip_add = proc_create_data (nfsfilt_proc_ip_add_name, 0600,
				nfsfilt_proc_dir, &nfsfilt_proc_ip_add_ops, NULL);
		if (nfsfilt_proc_status && nfsfilt_proc_ip_add) {
			return 0;
		}
	}
	nfsfilt_proc_cleanup ();
	return -ENOBUFS;
} /* nfsfilt_proc_init () */

/*** nfsfilt support functions ********************************************/

/*
 * check the length invariant part of the RPC header
 * return expected length of auth unix part of RPC
 */

static int
rpcfilt_check_hdr (struct nfsfilt_addr *client, struct rpc_call_hdr *rpc)
{
	DEBUGP(3, "RPC from %pI4, XID: 0x%x, type: %d, vers %d\n",
		&client->addr, ntohl (rpc->xid),
		ntohl (rpc->msg_type), ntohl (rpc->rpc_vers));

	if ((rpc->msg_type != htonl (RPC_CALL)) ||
		(rpc->rpc_vers != htonl (RPC_VERSION))) {
		DEBUGP(1, "RPC from %pI4 not NFS: msg_type: %d, rpc_vers: %d, rpc_prog %d\n",
			&client->addr, ntohl (rpc->msg_type), ntohl (rpc->rpc_vers),
			ntohl (rpc->prog));
		client->bad_nfs++;
		return -1;
	}

	if (rpc->flavor != htonl (RPC_AUTH_UNIX)) {
		DEBUGP(3, "RPC from %pI4 not auth unix: flavor: %d\n",
			&client->addr, ntohl (rpc->flavor));
		client->not_auth_unix++;
		return 0;	/* let the server deal with it */
	}

	return ntohl (rpc->length);
} /* rpcfilt_check_hdr () */

/*
 * filter an in-memory RPC call, checking unix auth credentials
 */

static unsigned int
rpcfilt_auth_unix_mangle (struct nfsfilt_addr *client, void *buf,
	int size, unsigned long *checksum)
{
	u32 *bufp = buf;
	u32 slen;
	u32 num_aux_gids;
	uid_t *uidp;
	gid_t *gidp;
	struct nfsfilt_auth *auth;
	unsigned long csum = *checksum;

	size <<= 2;					/* only care about u32's here */
	size -= MIN_UNIX_AUTH_WORDS;

	bufp++;						/* time stamp */
	slen = ((ntohl (*bufp++) + 3) >> 2);	/* machname length in words */
	if (slen > size) {
		DEBUGP(1, "RPC from %pI4 has bad machine name length\n", &client->addr);
		client->bad_namelen++;
		return -1;				/* DROP/REJECT */
	}
	bufp += slen;				/* skip machname */
	size -= slen;

	uidp = bufp++;
	gidp = bufp++;
	DEBUGP(2, "NFS packet from %pI4, with uid %d gid %d, machname len (words): %d\n",
		&client->addr, ntohl (*uidp), ntohl (*gidp), slen);

	num_aux_gids = ntohl (*bufp++);
	if (num_aux_gids > size) {
		DEBUGP(1, "RPC from %pI4 bad # aux gids: %d, size remaining: %d\n",
			&client->addr, num_aux_gids, size);
		client->bad_ngids++;
		nfsfilt_dump (buf, 3);
		return -1;		/* DROP/REJECT */
	}

#ifdef NO_ROOT_SQUASH
	/* let through requests from root - rely on server to deal with */
	if ((*uidp == 0) && (*gidp == 0)) {
		client->num_root++;
		return 0;		/* ALLOW */
	}
#endif

	/* check if valid uid in table */
	auth = &client->auth[0];
	while (auth != &client->auth[MAX_UIDS]) {
		if ((auth->num_gids > 0) && (auth->uid == *uidp)) break;
		auth++;
	}
	if (auth != &client->auth[MAX_UIDS]) {
		/* found a valid UID entry */
		int i;
		int change = 0;

		if (*gidp != auth->gid[0]) {
			DEBUGP(1, "modified invalid gid (%d) from %pI4\n",
					ntohl (*gidp), &client->addr);
			csum += CSUM_ADD16(~*gidp) + CSUM_ADD16(auth->gid[0]);
			*gidp = auth->gid[0];
			change = 1;
		}

		for (i = 0; i < num_aux_gids; i++) {
			int j;

			for (j = 0; (j < auth->num_gids) &&
					(*bufp != auth->gid[j]); j++);
			if (j == auth->num_gids) {
				DEBUGP(1, "modified invalid auxiliary gid %d"
					" (%d) from %pI4\n",
					i, ntohl (*bufp), &client->addr);
				csum += CSUM_ADD16(~*bufp) +
				       		CSUM_ADD16(auth->gid[0]);
				*bufp = auth->gid[0];
				change = 1;
			}
			bufp++;
		}
		if (change) {
			auth->num_changed++;
			auth->last_changed = jiffies;
		} else {
			auth->num_rpcs++;
			return 0;		/* ALLOW */
		}
	} else {
		/* we don't drop invalid uid packets - we mangle them! */
		int i;
		DEBUGP(2, "mangling invalid uid %d (gid %d) from %pI4\n",
			ntohl (*uidp), ntohl (*gidp), &client->addr);
		if (*uidp == htonl (0)) {
			client->num_root++;
		} else {
			client->bad_uid++;
		}
		csum += CSUM_ADD16(~*uidp) + CSUM_ADD16(htonl (safe_uid));
		*uidp = htonl (safe_uid);
		csum += CSUM_ADD16(~*gidp) + CSUM_ADD16(htonl (safe_gid));
		*gidp = htonl (safe_gid);

		for (i = 0; i < num_aux_gids; i++) {
			csum += CSUM_ADD16(~*bufp) + CSUM_ADD16(*gidp);
			*bufp++ = *gidp;
		}
	}

	csum = CSUM_ADD16(csum);
	csum = CSUM_ADD16(csum);
	*checksum = csum;
	return 1;		/* MANGLED */
} /* rpcfilt_auth_unix_mangle () */


#include <net/ip.h>
#include <net/tcp.h>
#include <net/route.h>
#include <net/dst.h>

/* Send RST reply */
/*
 * largely lifted from include/net/netfilter/ipv4/nf_reject.h
 */

static void
rpcfilt_send_reset (struct sk_buff *oskb)
{
	struct sk_buff *nskb;
	const struct iphdr *oiph = ip_hdr (oskb);
	struct iphdr *niph;
	const struct tcphdr *oth;
	struct tcphdr _oth, *nth;

	oth = skb_header_pointer (oskb, ip_hdrlen (oskb),
				 sizeof(_oth), &_oth);
	if (oth == NULL) return;

	/* No RST for RST. */
	if (oth->rst) return;

	nskb = alloc_skb (sizeof(struct iphdr) + sizeof(struct tcphdr) +
			 LL_MAX_HEADER, GFP_ATOMIC);
	if (!nskb) return;

	/* reserve room for Link Layer header */
	skb_reserve (nskb, LL_MAX_HEADER);

	skb_reset_network_header (nskb);
	niph = (struct iphdr *) skb_put (nskb, sizeof(struct iphdr));
	niph->version = 4;
	niph->ihl = sizeof(struct iphdr) / 4;
	niph->tos = 0;
	niph->id = 0;
	niph->frag_off = htons(IP_DF);
	niph->protocol = IPPROTO_TCP;
	niph->check	= 0;
	/* swap src & dest IP addresses */
	niph->saddr	= oiph->daddr;
	niph->daddr	= oiph->saddr;

	skb_reset_transport_header (nskb);
	nth = (struct tcphdr *) skb_put (nskb, sizeof(struct tcphdr));
	memset (nth, 0, sizeof(struct tcphdr));
	nth->doff = sizeof(struct tcphdr) / 4;
	/* swap src & dest ports */
	nth->source = oth->dest;
	nth->dest = oth->source;

	if (oth->ack)
		nth->seq = oth->ack_seq;
	else {
		nth->ack_seq = htonl(ntohl(oth->seq) + oth->syn + oth->fin +
				oskb->len - ip_hdrlen(oskb) - (oth->doff << 2));
		nth->ack = 1;
	}

	nth->rst = 1;
	nth->check = ~tcp_v4_check (sizeof(struct tcphdr), niph->saddr,
				    niph->daddr, 0);
	nskb->ip_summed = CHECKSUM_PARTIAL;
	nskb->csum_start = (unsigned char *)nth - nskb->head;
	nskb->csum_offset = offsetof (struct tcphdr, check);

	/* ip_route_me_harder expects skb->dst to be set */
	skb_dst_set_noref (nskb, skb_dst (oskb));

	nskb->protocol = htons(ETH_P_IP);

#if LINUX_VERSION_CODE < KERNEL_VERSION(4,2,0)
	if (ip_route_me_harder (nskb, RTN_UNSPEC))
#else
	if (ip_route_me_harder (NULL, nskb, RTN_UNSPEC))
#endif
		return kfree_skb (nskb);
//		goto free_nskb;

	niph->ttl = ip4_dst_hoplimit (skb_dst (nskb));

	/* "Never happens" */
	if (nskb->len > dst_mtu (skb_dst (nskb)))
		goto free_nskb;

	nf_ct_attach (nskb, oskb);

#ifdef CONFIG_BRIDGE_NETFILTER
	/* If we use ip_local_out for bridged traffic, the MAC source on
	 * the RST will be ours, instead of the destination's.  This confuses
	 * some routers/firewalls, and they drop the packet.  So we need to
	 * build the eth header using the original destination's MAC as the
	 * source, and send the RST packet directly.
	 */
	if (oskb->nf_bridge) {
		struct ethhdr *oeth = eth_hdr (oskb);
		nskb->dev = oskb->nf_bridge->physindev;
		niph->tot_len = htons(nskb->len);
		ip_send_check (niph);
		if (dev_hard_header (nskb, nskb->dev, ntohs(nskb->protocol),
				oeth->h_source, oeth->h_dest, nskb->len) < 0)
			goto free_nskb;
		dev_queue_xmit (nskb);
	} else
#endif
	/*
	 * found Linux version change with:
	 * cd include/net
	 * git log --pretty=short -u -L 124,124:ip.h
	 * git describe --all <commit id>
	 */
#if LINUX_VERSION_CODE < KERNEL_VERSION(4,3,0)
		ip_local_out (nskb);
#else
	/*
	 * FIX_ME!
	 * int ip_local_out(struct net *net, struct sock *sk, struct sk_buff *skb);

		ip_local_out (NULL,NULL,nskb);
	 */
#endif
	return;

  free_nskb:
	kfree_skb (nskb);
} /* rpcfilt_send_reset () */

/*
 * significant protocol error:
 *  - close the connection to the server
 *  - reset the connection to the client
 */

static unsigned int
rpcfilt_tcp_reset (struct sk_buff *skb, struct nfsfilt_tcprpc *conn)
{
	struct iphdr *iph = ip_hdr (skb);
	if (iph->protocol == IPPROTO_TCP) {
		struct tcphdr *th = tcp_hdr (skb);
		int len = (iph->ihl + th->doff) * 4;

		conn->dest = 0;
		rpcfilt_send_reset (skb);

		/* throw away bad data in packet */
		skb_trim (skb, len);
		iph->tot_len = htons (len);

		th->fin = 1;
		th->check = ~tcp_v4_check (sizeof(struct tcphdr), iph->saddr,
				iph->daddr, 0);
		skb->ip_summed = CHECKSUM_PARTIAL;
		skb->csum_start = (unsigned char *)th - skb->head;
		skb->csum_offset = offsetof (struct tcphdr, check);
	}
	return XT_CONTINUE;
} /* rpcfilt_tcp_reset () */

/*** nfsfilt module target ***********************************************/

/*
 * The netfilter hook where the work gets done. Should be called from
 * FORWARDING, changes may have been made to the packet.
 */
static unsigned int
nfsfilt_tg (struct sk_buff *skb, const struct xt_action_param *par)
{
	struct iphdr *iph = ip_hdr (skb);
	struct nfsfilt_addr *client;
	int offset;
	void *rpc;
	int rpc_auth_len;
	int remain;
	unsigned long csum = 0;
	unsigned int retval;
	unsigned char buffer [128];

	DEBUGP(4, "packet from: %pI4, len: %d, ip size: %d, head: %p, data_len: %d\n",
		&iph->saddr, skb->len, ntohs (iph->tot_len), skb->head, skb->data_len);

	/* check if we know about this IP address */
	client = nfsfilt_hashtable [nfsfilt_hash (ntohl (iph->saddr))];
	while (client && (client->addr != iph->saddr))
		client = client->next;

	if (client == NULL) {
		DEBUGP(1, "Packet from unknown addr: %pI4\n", &iph->saddr);
		nfsfilt_nbad_addr++;
		return NF_DROP;
	}

	offset = iph->ihl * 4;

	if (iph->protocol == IPPROTO_UDP) {
		struct udphdr *uh = udp_hdr (skb);

		offset += sizeof (struct udphdr);
		DEBUGP(4, "UDP RPC from %pI4:%u, size: %d, rpc offset: %d\n",
			&iph->saddr, uh->source, skb->len, offset);

		rpc = skb_header_pointer (skb, offset, sizeof (struct rpc_call_hdr), buffer);
		if (rpc == NULL) {
			DEBUGP(1, "RPC from %pI4 too short: %d bytes\n", &client->addr, skb->len);
			client->bad_rpcsize++;
			return NF_DROP;
		}

		rpc_auth_len = rpcfilt_check_hdr (client, rpc);
		if (rpc_auth_len < 0) return NF_DROP;
		if (rpc_auth_len == 0) return XT_CONTINUE;

		offset += sizeof (struct rpc_call_hdr);
		rpc = skb_header_pointer (skb, offset, rpc_auth_len, buffer);

		csum = uh->check ^ 0xffff;

		retval = rpcfilt_auth_unix_mangle (client, rpc, rpc_auth_len, &csum);
		if (retval < 0) return NF_DROP;
		if (retval > 0) {
			/* mangled, so update the checksum (can't be 0: so make -0) */
			uh->check = (~csum) ? ~csum : csum;
		}
		return XT_CONTINUE;
	} else if (iph->protocol == IPPROTO_TCP) {
		struct tcphdr *th = tcp_hdr (skb);
		u32 next_seq = ntohl (th->seq);
		struct nfsfilt_tcprpc *conn = &client->servs[0];
		int mangled = 0;

		offset += th->doff * 4;
		/* poor man's connection tracking - find TCP server entry */
		while (conn != &client->servs[MAX_TCP_SERVERS]) {
			if ((conn->dest == iph->daddr) &&
				(conn->dport == th->dest) &&
				(conn->sport == th->source)) break;
			conn++;
		}

		remain = skb->len - offset;
		if (remain <= 0) {
			client->tcp_handshake++;
			if (conn == &client->servs[MAX_TCP_SERVERS]) {
				if (th->syn && !th->ack && !th->rst && !th->fin) {
					/* find an empty entry */
					conn = &client->servs[0];
					while ((conn != &client->servs[MAX_TCP_SERVERS]) &&
						(conn->dest != 0)) conn++;
					if (conn == &client->servs[MAX_TCP_SERVERS]) {
						DEBUGP(0, "out of TCP server entries from: %pI4 to: %pI4\n",
							&iph->saddr, &iph->daddr);
						client->bad_nserv++;
						return NF_DROP;
					} 
					/* new connection */
					conn->dest = iph->daddr;
					conn->nseq = next_seq + 1;
					conn->dport = th->dest;
					conn->sport = th->source;
					DEBUGP(3, "new TCP connection from: %pI4:%u to %pI4:%u\n",
						&iph->saddr, htons (th->source),
						&iph->daddr, htons (th->dest));
				}
			} else if (th->fin || th->rst) {
				DEBUGP(3, "closing TCP connection from: %pI4:%u to %pI4:%u\n",
					&iph->saddr, htons (th->source),
					&iph->daddr, htons (th->dest));
				memset (conn, 0, sizeof(struct nfsfilt_tcprpc));
			}
			/* let everything else go through - no data so can't hurt */
			return XT_CONTINUE;
		}

		if (conn == &client->servs[MAX_TCP_SERVERS]) {
			DEBUGP(2, "untracked TCP packet from: %pI4:%u to %pI4:%u\n",
				&iph->saddr, htons (th->source),
				&iph->daddr, htons (th->dest));
			return NF_DROP;
		}

		DEBUGP(3, "TCP RPC from %pI4:%u, expect seq no.: %u, seq no.: %u, diff: %u, remain: %d\n",
			&iph->saddr, th->source, conn->nseq, next_seq, conn->nseq - next_seq, remain);

		remain -= (u32)(conn->nseq - next_seq);
		csum = th->check ^ 0xffff;
		while (remain > 0) {
			u32 fraghdr;

			offset = skb->len - remain;
#define MIN_RPC (sizeof (fraghdr) + sizeof (struct rpc_call_hdr))
			rpc = skb_header_pointer (skb, offset, MIN_RPC, buffer);
			if (rpc == NULL) {
				DEBUGP(1, "TCP RPC from %pI4 too short by: %ld bytes\n",
					&client->addr, MIN_RPC - remain);
				client->bad_rpcsize++;
				return rpcfilt_tcp_reset (skb, conn);
			}

			fraghdr = ntohl(*(u32 *)rpc);
			rpc += sizeof (fraghdr);

			if (!(fraghdr & RPC_LAST_STREAM_FRAGMENT)) {
				/* we don't support multiply fragmented RPC */
				client->bad_frags++;
				DEBUGP(1, "TCP RPC from %pI4 - not last fragment\n",
					&client->addr);
				return rpcfilt_tcp_reset (skb, conn);
			}
			fraghdr &= RPC_FRAGMENT_SIZE_MASK;
			conn->nseq += sizeof(fraghdr) + fraghdr;
			remain -= sizeof(fraghdr) + fraghdr;

			DEBUGP(3, "TCP RPC from %pI4, remaining bytes: %d\n",
				&client->addr, skb->len - offset);

			offset += MIN_RPC;
			rpc_auth_len = rpcfilt_check_hdr (client, rpc);
			DEBUGP(4, "TCP RPC from %pI4, rpcfilt_check_hdr returned %d\n",
				&client->addr, rpc_auth_len);
			if (rpc_auth_len < 0) {
				DEBUGP(1, "TCP RPC from %pI4, rpcfilt_check_hdr returned %d\n",
					&client->addr, rpc_auth_len);
				return rpcfilt_tcp_reset (skb, conn);
			}
			if (rpc_auth_len == 0) continue;

			rpc = skb_header_pointer (skb, offset, rpc_auth_len, buffer);
			if (rpc == NULL) {
				DEBUGP(1, "RPC auth len from %pI4 too short by: %d bytes\n",
					 &client->addr, rpc_auth_len - (skb->len - offset));
				client->bad_rpcsize++;
				return rpcfilt_tcp_reset (skb, conn);
			}

			retval = rpcfilt_auth_unix_mangle (client, rpc, rpc_auth_len, &csum);
			if (retval == -1) return rpcfilt_tcp_reset (skb, conn);
			if (retval == 0) continue;

			DEBUGP(4, "skb is%s cloned\n", skb_cloned (skb) ? "" : " not");
			/* auth data has been mangled, need to make skb writable */
			if (skb_linearize_cow (skb)) {
				DEBUGP(1, "TCP RPC from %pI4, unable to linearize skb\n",
					&client->addr);
				return rpcfilt_tcp_reset (skb, conn);
			}
			mangled = 1;
			if (rpc == buffer) {
				memcpy (skb->data + offset, buffer, rpc_auth_len);
			}
		}
		if (mangled) {
			/* if skb has been linearized, former th is probably invalid */
			th = tcp_hdr (skb);
			/* update the checksum (can't be 0: so make -0) */
			th->check = (~csum) ? ~csum : csum;
		}
	} else {
		client->bad_proto++;
	}
	return XT_CONTINUE;
} /* nfsfilt_tg () */

static int nfsfilt_tg_check (const struct xt_tgchk_param *par)
{
	return 0;	// 0 => everything OK, anything else is an error...
} /* nfsfilt_tg_check () */

/*** nfsfilt module init/fini *******************************************/
static struct xt_target nfsfilt_xt_tg = {
	.name		= "NFSFILT",
	.family		= NFPROTO_IPV4,
	.target		= nfsfilt_tg,
	.targetsize = sizeof(struct nfsfilt_xt_info),
	.table      = "mangle",
	.checkentry	= nfsfilt_tg_check,
	.me		= THIS_MODULE,
};

static int __init init (void)
{
	int i;
	for (i = 0; i < MAX_HASH; i++) nfsfilt_hashtable [i] = NULL;

	if (nfsfilt_proc_init () < 0) return -1;

	if (xt_register_target (&nfsfilt_xt_tg) == -1) {
		nfsfilt_proc_cleanup ();
		return -EINVAL;
	}
#ifdef DEBUG
	DEBUGP(1, "dbglvl: %d, using safe uid: %d, safe gid: %d\n", dbglvl, safe_uid, safe_gid);
	DEBUGP(3, "Size of RPC header: %ld bytes\n", sizeof(struct rpc_call_hdr));
#endif
	return 0;
} /* init () */

static void __exit fini (void)
{
	xt_unregister_target (&nfsfilt_xt_tg);
	nfsfilt_proc_cleanup ();
	nfsfilt_mem_free ();
} /* fini () */

module_init (init);
module_exit (fini);
