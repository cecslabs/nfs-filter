#!/bin/bash

# flush out the mangle table
iptables -t mangle -F
rmmod xt_NFSFilt
insmod xt_NFSFilt.ko dbglvl=3
# insmod xt_NFSFilt.ko
iptables -t mangle -A FORWARD -s 192.168.0.0/16 --protocol udp --dport 2049 -j NFSFILT
iptables -t mangle -A FORWARD -s 192.168.0.0/16 --protocol tcp --dport 2049 -j NFSFILT
