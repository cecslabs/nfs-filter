NFS Filter module iptables extensions

Once the NFS Filter kernel module is loaded into the kernel, it needs to be configured with the iptables command.

To enable the iptables userspace command to know about the NFS Filter mangle option, it needs to be rebuilt with the extensions in this directory added.

Simply copy the extensions/* files into an iptables source tree (tested with 1.4.12) and rebuild iptables.

Alternatively, build a new .deb package using the Makefile and the add_extensions.sh script. To do this, you need to know the full name of the package to build.

'apt-cache show iptables | grep Version' will find the version of iptables on the host system. Then use:
make iptables_${VERSION}~NFSFILT_amd64.deb

which will download the iptables source (hopefully matching version $VERSION) and then patch it and rebuild it into a .deb file.

