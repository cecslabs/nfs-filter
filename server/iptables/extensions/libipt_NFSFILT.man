This target checks the UID and GIDs of a UDP NFSv2 packet for a specific
source IP address against a kernel table. If the UID or any GIDs do not match,
then the UID and GIDs are set to "benign" values:
.TP
.BR "--queue-num " "\fIvalue"
The /proc/nfs_filter/table_add file lists the entries in the kernel table and
writing a line of text as "A.B.C.D uid gid gid ..." to the file will add that
entry.
.TP
The /proc/nfs_filter/table_del file is used to delete entries from the table.
