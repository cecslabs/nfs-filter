/* Shared library add-on to iptables to add NFSFILT target support. */
#include <xtables.h>
#include <stdio.h>

/* Function which prints out usage message. */
static void NFSFILT_help(void)
{
	printf(
"NFSFILT target v%s takes no options\n",
XTABLES_VERSION);
}

static struct xtables_target nfsfilt_tg_reg = {
	.name		= "NFSFILT",
	.version	= XTABLES_VERSION,
	.family		= NFPROTO_IPV4,
	.size		= XT_ALIGN(8),
	.userspacesize	= XT_ALIGN(8),
	.help		= &NFSFILT_help
};

void _init(void)
{
	xtables_register_target(&nfsfilt_tg_reg);
}
