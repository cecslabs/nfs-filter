#
# manually add our files as a "patch" to iptables package and rebuild
#
# by Robert Edwards <bob@cs.anu.edu.au>

# who can be bothered learning how to make quilt work in a non-interactive script...

[ -z $1 ] && echo "need a target" && exit 1

PKG=${1%%_*}
echo pkg:${PKG}
PKG_VERS=${1#*_}
SUFFIX="NFSFILT"
PKG_VERS=${PKG_VERS%%.NFSFILT*}
SHORT=${PKG_VERS%%-*}
echo version ${PKG_VERS}
echo short: ${SHORT}
T_DIR=${PKG}-${SHORT}
SUFFIX=${SUFFIX%%_*}
PATCHFILE=0999-extension_${SUFFIX}.patch

echo suffix ${SUFFIX}
echo patchfile ${PATCHFILE}
echo target_dir ${T_DIR}

DATE=`date -R`

# http://people.connexer.com/~roberto/howtos/debcustomize
grep -q ${SUFFIX} ${T_DIR}/debian/changelog
if [ $? -eq 1 ] ; then
  # update changelog
  cat > new_change << EOF
iptables (${PKG_VERS}.${SUFFIX}) buster; urgency=low

  * Non-maintainer upload
  * added NFSFILT target support

 -- Bob Edwards <bob@cs.anu.edu.au>  ${DATE}

EOF
  cat new_change ${T_DIR}/debian/changelog > changelog
  cp changelog ${T_DIR}/debian
fi

# diff is stupid - create the patch file manually...
# http://dep.debian.net/deps/dep3/
cat > ${PATCHFILE} << EOF2
Description: add NFSFILT target to iptables command
Author: Bob Edwards <bob@cs.anu.edu.au>
Last-Update: ${DATE}

EOF2

for i in `ls extensions` ; do
  PFILE=extensions/${i}
  LEN=`cat ${PFILE} | wc -l`
  cat >> ${PATCHFILE} << EOF3
--- a/${PFILE}
+++ b/${PFILE}
@@ -0,0 +1,${LEN} @@
EOF3
  sed 's/^/+/' ${PFILE} >> ${PATCHFILE}
done

cp ${PATCHFILE} ${T_DIR}/debian/patches
grep -q ${PATCHFILE} ${T_DIR}/debian/patches/series
[ $? -eq 1 ] && echo ${PATCHFILE} >> ${T_DIR}/debian/patches/series

# go ahead and build it...
cd ${T_DIR}
debuild -us -uc

