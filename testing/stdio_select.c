/*
*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/select.h>

int readwrite (int in, int out) {
	char buf[100];
	int num;

	num = read (in, buf, 99);
	if (num > 0) {
		write (out, buf, num);
	}
	return (num);
} /* readwrite () */

int main (int argc, char *argv[]) {
	fd_set rfds;
	int fd;
	int retval;
	int finished = 0;

	if (argc < 2) {
		printf ("Need a file to read/write\n");
		exit (1);
	}

	fd = open (argv[1], O_RDWR);
	if (fd < 0) {
		printf ("open failed\n");
		exit (1);
	}

	while (!finished) {
		FD_ZERO (&rfds);
		FD_SET (0, &rfds);
		FD_SET (fd, &rfds);
		retval = select (fd + 1, &rfds, NULL, NULL, NULL);
		
		if (retval > 0) {
			if (FD_ISSET (0, &rfds)) {
				if (readwrite (0, fd) == 0) {
					finished = 1;
				}
			} else if (FD_ISSET (fd, &rfds)) {
				if (readwrite (0, fd) == 0) {
					finished = 1;
				}
			}
		}
	}

	close (fd);
	return (0);
} /* main () */
