/**
 **
 ** conf.h - function prototypes for the config handling routines
 **
 **/
#include <stdlib.h>
#include <stdio.h>

#ifndef CONFIG_H
#define CONFIG_H

#define DEBUG_FILE "/var/log/nfs_authd.log"
#define CONF_PATH_LEN 255
/* 
    more parameters may be added later.
 */
struct config {
    int daemon_mode;
    char config_file[CONF_PATH_LEN + 1];
    char server[CONF_PATH_LEN + 1];
    int port;
};

extern struct config config;                      

#define CONFIG_STRING  0x01
#define CONFIG_INTEGER 0x02
#define CONFIG_BOOLEAN 0x04

struct config_setup{
    char *param_name;
    char *comment;
    void *variable;
    char *default_value;
    int type;
};

int config_load (char *conf_file);
void config_copy(int param_number, char *new_value);
void config_cmdparse(char *cmd, char *arg1);
void config_print();
void config_defaults (void);

#endif
