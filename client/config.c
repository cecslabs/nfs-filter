/*
  **
  ** config.c
  **
  ** Part of the drpoxy package by Matthew Pratt. 
  **
  ** Copyright 2000 Matthew Pratt <mattpratt2yahoo.com>
  **
  ** This software is licensed under the terms of the GNU General 
  ** Public License (GPL). Please see the file COPYING for details.
  ** 
  **
*/

#include <sys/types.h>
#include <string.h>
#include "config.h"

struct config_setup config_params[] ={
    {
#define DEFAULT_SERVER "192.168.1.1"
	"server",
	"# The IP of host that the NFS filter sevrer demon is running on.\n"
	"# Typically this will be the same host as the default gaetway.\n",
	(void *)&config.server,
	DEFAULT_SERVER,
	CONFIG_STRING
    },
    {
#define DEFAULT_PORT "8787"
	"port" ,
	"# The port number that the NFS filter server is running on.\n",
	(void *)&config.port,
	DEFAULT_PORT,
	CONFIG_INTEGER
    },
    /*
     * end-of-array indicator
     */
    { NULL, NULL, NULL, NULL, 0 }
};


struct config config;  

/**************************************************************************
    Main function
*/
int config_load (char *conf_file)
{
    FILE *fp;
    char line[CONF_PATH_LEN + 1], *cmd = NULL, *arg1 = NULL;
    
    config_defaults();	/* load default settings first */
    
    fp = fopen (conf_file, "r");
    if (!fp) {	
	perror("no config file");
	return 0;
    } else {
	strcpy(config.config_file, conf_file);
    }
    while (fgets(line, CONF_PATH_LEN, fp)) {
	if (!(line[0]=='#')) {	/* skip lines with comment */
	    line[strlen(line) - 1] = 0; /* kill '\n' */
	    cmd = strtok( line, " =" );
	    /* now we have cmd, but we need the rest of the line as arg1 */
	    if( cmd != NULL ){
		arg1 = &cmd[ strlen(cmd) + 1 ]; //arg1 point to end of cmd
		while( *arg1 && *arg1 != ' ' )arg1++; //get rid of and spaces
		if( *arg1 == ' ' )arg1++;
	    }
	    config_cmdparse(cmd, arg1);
	}
    }
    fclose(fp);
    return 0;
}
/*****************************************************************************/
void config_copy (int param_number, char *new_value)
{
    switch (config_params[param_number].type) {
	case CONFIG_STRING:
	    if (strlen (new_value) > CONF_PATH_LEN) new_value[CONF_PATH_LEN] = 0;
	    strcpy ((char *) config_params[param_number].variable, new_value);
	    break;
	case CONFIG_INTEGER:
	    *((int *)config_params[param_number].variable) = atoi(new_value);
	    break;
	case CONFIG_BOOLEAN:
	    if ( !strcmp(new_value, "1") || 
		 !strcasecmp(new_value, "yes") || 
		 !strcasecmp(new_value,"on")) {
		*((int *)config_params[param_number].variable) = 1;
	    }else
		*((int *)config_params[param_number].variable) = 0;
	    break;
	default:
	    fprintf(stderr, "Unkown parameter type\n");
	}
}
/*****************************************************************************/
void config_cmdparse(char *cmd, char *arg1)
{
    int i = 0;
    
    if( !cmd )return;
    if( !arg1 )return;
    
    while( config_params[i].param_name != NULL ) {
	if(!strncasecmp(cmd, config_params[i].param_name , 
			CONF_PATH_LEN + 50)) 
	    {
		config_copy( i, arg1 );
		return;
	    }
	i++;
    }
    
    fprintf( stderr, "Unkown config option: \"%s\"\n", cmd ); 
    return;
}


/************************************************************************
 * print the configuration on stdout.
************************************************************************/
void config_print() {
   int i = 0;
   FILE * fd = stdout;

   fprintf( fd, "# This is the configuration file for nfs_authd by\n");
   fprintf( fd, "# Matthew Pratt <mattpratt@yahoo.com>.\n");
   fprintf( fd, "#\n");
   fprintf( fd, "# NOTE: The maximum length of each option is limited. This limit is set\n");
   fprintf( fd, "# at compile time using  CONF_PATH_LEN in config.h in the source.\n");
   fprintf( fd, "#    CONF_PATH_LEN is currently set to %d bytes\n", CONF_PATH_LEN );
   fprintf( fd, "#\n");

   while( config_params[i].param_name != NULL ) {
	fprintf(fd,"# param %s \n" , config_params[i].param_name );
	fprintf(fd, "%s", config_params[i].comment);
	fprintf(fd, "#\n");
	fprintf(fd, "# Default: %s", config_params[i].default_value );
	fprintf(fd, "\n#\n");
	fprintf(fd, "%s = %s" , config_params[i].param_name,  
		                config_params[i].default_value );
	fprintf(fd, "\n\n");

	i++;
   }
}
/************************************************************************
    Load default settings first
*/
void config_defaults (void)
{
    int i = 0;

    for( i = 0; config_params[i].param_name != NULL; i++)
	config_copy( i, config_params[i].default_value );

    config.daemon_mode = 1;
    return;
}

