#!/bin/bash

[ -z "$1" ] && echo "need user" && exit 1

LOCALFS=/students
IPNET=`hostname -I | cut -d '.' -f 3`
MOUNTOPTS="async,hard,vers=3,nodev,nosuid"
MOUNT=/bin/mount

USERNAME=$1
SERVER_NUM=${1: -1}
STUDENT="csitnfs2.csitlabs:/socofs/${SERVER_NUM}"
COURSES="csitnfs2.csitlabs:/courses"

mkdir -p /scratch/students/$USERNAME/
chown $USERNAME:student /scratch/students/$USERNAME/
chmod 700 /scratch/students/$USERNAME/

chmod a+rw /dev/kvm

pushd /usr/local/chapel
  source util/quickstart/setchplenv.bash
popd

if [ -d /lab/etc/exammode/$IPNET ] 
then
  exec /lab/etc/exammode/$IPNET/session $1
fi

# if already mounted then dont mount it again
MOUNTS=`grep $1 /proc/mounts`
[ -n "$MOUNTS" ] && exit 0

mkdir -p $LOCALFS/$USERNAME
mkdir -p /courses

$MOUNT -t nfs -o rw,$MOUNTOPTS -v $STUDENT/$USERNAME $LOCALFS/$USERNAME 2>&1 | /usr/bin/logger -tsession_open.sh
$MOUNT -t nfs -o ro,relatime,vers=3,rsize=524288,wsize=524288,namlen=255,hard,proto=tcp,timeo=600,retrans=2 -v $COURSES /courses 2>&1  | /usr/bin/logger -tsession_open.sh

sudo -u $USERNAME mkdir -p $LOCALFS/$USERNAME/.cache 2>&1  | /usr/bin/logger -tsession_open.sh
$MOUNT -t tmpfs -o size=2G tmpfs $LOCALFS/$USERNAME/.cache 2>&1 | /usr/bin/logger -tsession_open.sh

# sudo -u $USERNAME rm -rf $LOCALFS/$USERNAME/scratch 2>&1  | /usr/bin/logger -tsession_open.sh
sudo -u $USERNAME mkdir -p $LOCALFS/$USERNAME/scratch 2>&1  | /usr/bin/logger -tsession_open.sh
$MOUNT --bind /scratch/students/$USERNAME/ $LOCALFS/$USERNAME/scratch 2>&1 | /usr/bin/logger -tsession_open.sh

