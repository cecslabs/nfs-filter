#!/bin/bash

# This is where the student home directories are mounted
# do some logging so we can tell what is going on
echo "closing session for username $1">/tmp/umount.log

# Check that we were given an argument (the user name)
if [ -z "$1" ];then
    echo "need user";
    exit 1;
fi

IPT=`which iptables`

# Reset all
$IPT -F
$IPT -X

ps -f --no-headers -u $1 | /usr/bin/logger -tsession.close.prekill

killall -u $1 -s SIGKILL

ps -f --no-headers -u $1 | /usr/bin/logger -tsession.close.postkill

for mount in `grep $1 /proc/mounts | cut -f2 -d' '`
do
  umount -f $mount 2>&1 | /usr/bin/logger -tsession.close
  sleep 0.2
done

umount -f /students/$1 2>&1 | /usr/bin/logger -tsession.close
sleep 0.2
umount -f /courses 2>&1 | /usr/bin/logger -tsession.close

find /tmp -user $1 -type f -delete

if [ -f /lab/current ]
then
  SRV_REL=`cat /lab/current`
  CUR_REL=`cat /etc/csit_release`
  if [ ${SRV_REL} != ${CUR_REL} ] 
  then
    sleep 1
    reboot
  fi
fi

