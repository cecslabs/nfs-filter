/**
 ** commandline.c - a login program for the NFS filter package by Matthew Pratt
 **
 ** This program is basically a copy of su. The main difference is that this
 ** program ask for authenication from root as well as from any other user.
 ** This is done to ensure that a pam session will be opened by the nfs_filter
 ** pam authenication module.
 **
 ** Ordinary su cannot be used in its place as su-ing from root would not start
 ** the pam_nfs_filter.so authentication process and so the nfs_filter server
 ** would not know about us.
 **
 **/

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <security/pam_appl.h>
#include <security/pam_misc.h>
#include <sys/types.h>
#include <sys/wait.h>

#define MESSAGE "Your home directory should now be mounted.\n"\
"Closing this shell will terminate the mount\n"

static struct pam_conv conv = {
    misc_conv,
    NULL
};

int main()
{
    pid_t parent;
    pam_handle_t *pamh;
    int result;
    char user[128];
    int i;

    printf("Username: ");fflush(stdout);
    fgets(user, sizeof(user), stdin);
    for(i = 0; user[i]; i++)
	if(user[i] == '\r' || user[i] == '\n')user[i] = 0;

    result = pam_start("login", user, &conv, &pamh);
    if( result != PAM_SUCCESS){
	printf("Could not open PAM session for nfs_filter\n");
	goto failure;
    }
    
    result = pam_authenticate(pamh, 0);
    if(result != PAM_SUCCESS){
	printf("Authentication FAILED\n");
	goto failure;
    }

    result = pam_open_session(pamh,0);
    if (result != PAM_SUCCESS) {
	fprintf (stderr, "could not open session\n");
	goto failure;
    }

    parent = fork();
    if(parent == -1){
	fprintf(stderr, "Cound not fork for a shell\n");
	goto failure;
    }
    
    if(!parent){ /* child only */
	fprintf(stderr, MESSAGE);
	execlp("/bin/bash", "bash", NULL);
	printf("Exec failed\n");
	perror("exec bash");
	_exit(1);
    }

    waitpid(-1, NULL, WUNTRACED);
    result = pam_close_session(pamh, 0);
    pam_end(pamh,result);
    return 0;

  failure:
    printf("PAM: %s\n", pam_strerror(pamh, result));
    pam_end(pamh,result);
    return 1;
}





