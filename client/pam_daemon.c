/**********************************************************************
 * pam_daemon.c - pam module to link to a daemon over Unix Domain Socket
 *
 * Robert Edwards, June 2006
 */

#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/stat.h> 
#include <sys/types.h>
#include <sys/un.h> 

#define PAM_SM_AUTH 
#define PAM_SM_SESSION 

#include <security/pam_ext.h>
#include <security/pam_modules.h>  
#include <security/_pam_macros.h>

#define PAM_DAEMON_DEFSOCK "/var/run/pam_daemon_socket"

int uds_connect (const char *sockpath) {
	int sock;
	int len;
	struct sockaddr_un serv;

	if (!sockpath || (!*sockpath))
		return -1;
	len = strlen (sockpath);
	if (len > (sizeof (serv.sun_path) - 1)) 
		return -1;

	if ((sock = socket (PF_UNIX, SOCK_STREAM, 0)) == -1) {
		return -1;
	}

	serv.sun_family = AF_UNIX;
	strcpy (serv.sun_path, sockpath);

	if (connect (sock, (struct sockaddr *) &serv,
		sizeof (serv.sun_family) + len) == -1) {
		close (sock);
		return -1;
	}
	return sock;
} /* uds_connect () */

#define PAM_DEBUG_ARG 1
#define UNIX_PATH_LEN 108

static int _pam_parse (pam_handle_t *pamh, int argc, const char **argv, char *sockpath) {
	int retval = 0;

	for (; argc-- > 0; ++argv) {
		if (strcmp (*argv, "debug") == 0) 
			retval |= PAM_DEBUG_ARG;
		else if ((strncmp (*argv, "sockpath=", 9) == 0) &&
			(strlen (*argv) < (UNIX_PATH_LEN + 9)))
			strcpy (sockpath, *argv+9);
		else
			pam_syslog (pamh, LOG_ERR, "pam_parse: unknown option: %s", *argv);
	}
	return retval;
} /* _pam_parse () */

static int _pam_daemon_connect (pam_handle_t *pamh, int debug, const char *msg, const char *sockpath) {
	int sock;
	fd_set rfds;
	struct timeval tv;
	int finished = 0;
	char buf[1024];
	int nread;
	int retval = PAM_AUTH_ERR;

	if ((sock = uds_connect (sockpath)) == -1) {
		pam_syslog (pamh, LOG_ERR, "connect error: uds_connect: %s",
			strerror (errno));
		return PAM_AUTH_ERR;
	}

	if (send (sock, msg, strlen (msg) + 1, 0) <= strlen (msg)) {
		close (sock);
		pam_syslog (pamh, LOG_ERR, "send error");
		return PAM_AUTH_ERR;
	}
    
	while (!finished) {
		/* set the (three second) time out */
		tv.tv_sec = 5;
		tv.tv_usec = 0;
		FD_ZERO(&rfds);
		FD_SET(sock, &rfds);

		if (select (FD_SETSIZE, &rfds, NULL, NULL, &tv)) {
			if (FD_ISSET(sock, &rfds)) {
				nread = recv (sock, buf, sizeof (buf), 0);
				if (debug) pam_syslog (pamh, LOG_INFO, "pam_daemon_connect got response: %d bytes", nread);
				if (nread > 0) {
					buf[nread] = 0;
					if (strncmp (buf, "SUCCESS", 7) == 0) {
						retval = PAM_SUCCESS;
					}
				}
				finished = 1;
			}
		} else { /* timeout */
			finished = 1;
		}
	}
	close (sock);
	return retval;
} /* _pam_daemon_connect */

static int get_password (pam_handle_t *pamh) {
	char *resp;
	const void *item;
	int retval;

	retval = pam_prompt (pamh, PAM_PROMPT_ECHO_OFF, &resp, ("Password: "));
	if (retval != PAM_SUCCESS) return retval;
	if (resp == NULL) return PAM_CONV_ERR;

	retval = pam_set_item (pamh, PAM_AUTHTOK, resp);

	/* make sure no copy of password left in memory */
	_pam_overwrite (resp);
	_pam_drop (resp);

	if ( (retval != PAM_SUCCESS) ||
	    ((retval = pam_get_item (pamh, PAM_AUTHTOK, &item)) != PAM_SUCCESS)) return retval;

	return retval;
}  /* get_password () */

PAM_EXTERN int pam_sm_authenticate (pam_handle_t *pamh, int flags,
					int argc, const char **argv) {
	int args;
	const char *user;
	const void *pass;
	int retval;
	char sockpath[UNIX_PATH_LEN] = PAM_DAEMON_DEFSOCK;
	char message[1024];
	int debug;

	args = _pam_parse (pamh, argc, argv, sockpath);
	debug = args & PAM_DEBUG_ARG;
	if (debug) pam_syslog (pamh, LOG_DEBUG, "started with args=%d", args);

	retval = pam_get_user (pamh, &user, NULL);
	if ((user == NULL) || (retval != PAM_SUCCESS)) {
		pam_syslog (pamh, LOG_ERR, "no username!");
		return PAM_AUTH_ERR;	 /* How did we get authenticated?! */
	}

	/* Check if we got a password */
	retval = pam_get_item (pamh, PAM_AUTHTOK, &pass);
	if (pass == NULL || retval != PAM_SUCCESS) {
		retval = get_password (pamh);
		if (retval != PAM_SUCCESS) {
			pam_syslog(pamh, LOG_ERR, "can not obtain password from user");
			return retval;
		}

		retval = pam_get_item(pamh, PAM_AUTHTOK, &pass);
        	if (pass == NULL || retval != PAM_SUCCESS) {
			pam_syslog(pamh, LOG_ERR, "can not recover user password");
			return PAM_AUTHTOK_RECOVERY_ERR;
		}
	}

	snprintf (message, sizeof(message), "%d\tauth\t%s\t%s\n", getpid (), user, (char *) pass);
	if (debug)
		pam_syslog (pamh, LOG_DEBUG, "authenticate sockpath:%s message:%s", sockpath, message);
	return (_pam_daemon_connect (pamh, debug, message, sockpath));
}

PAM_EXTERN int pam_sm_open_session (pam_handle_t *pamh, int flags,
					int argc, const char **argv) {
	int args;
	char *user;
	int retval;
	char sockpath[UNIX_PATH_LEN] = PAM_DAEMON_DEFSOCK;
	char message[1024];
	int debug;

	args = _pam_parse (pamh, argc, argv, sockpath);
	debug = args & PAM_DEBUG_ARG;
	if (debug) pam_syslog (pamh, LOG_DEBUG, "started with args=%d", args);

	retval = pam_get_item (pamh, PAM_USER, (void *) &user);
	if ((user == NULL) || (retval != PAM_SUCCESS)) {
		pam_syslog (pamh, LOG_ERR, "no username!");
		return PAM_AUTH_ERR;	 /* How did we get authenticated?! */
	}

	snprintf (message, sizeof(message), "%d\topen\t%s\n", getpid (), user);
	if (debug)
		pam_syslog (pamh, LOG_DEBUG, "open_session sockpath:%s message:%s", sockpath, message);
	return (_pam_daemon_connect (pamh, debug, message, sockpath));
}

PAM_EXTERN int pam_sm_close_session (pam_handle_t *pamh, int flags,
				int argc, const char **argv) {
	int args;
	char *user;
	int retval;
	char sockpath[UNIX_PATH_LEN] = PAM_DAEMON_DEFSOCK;
	char message[1024];
	int debug;

	args = _pam_parse (pamh, argc, argv, sockpath);
	debug = args & PAM_DEBUG_ARG;
	if (debug) pam_syslog (pamh, LOG_DEBUG, "started with args=%d", args);

	retval = pam_get_item (pamh, PAM_USER, (void *) &user);
	if ((user == NULL) || (retval != PAM_SUCCESS)) {
		pam_syslog (pamh, LOG_ERR, "no username!");
		return PAM_AUTH_ERR;	 /* How did we get authenticated?! */
	}

	snprintf (message, sizeof(message), "%d\tclose\t%s\n", getpid (), user);
	if (debug)
		pam_syslog (pamh, LOG_DEBUG, "close_session sockpath:%s message:%s", sockpath, message);
	return (_pam_daemon_connect (pamh, debug, message, sockpath));
}

PAM_EXTERN int pam_sm_setcred (pam_handle_t *pamh, int flags,
				int argc, const char **argv) {
	return PAM_SUCCESS;
}

PAM_EXTERN int pam_sm_acct_mgmt (pam_handle_t *pamh, int flags,
				int argc, const char **argv) {
	return PAM_SUCCESS;
}
