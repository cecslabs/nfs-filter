/**********************************************************************
 * ssl_authd.c - daemon process for listening to auth requests
 *
 * Matt Pratt and Bob Edwards
 * Department of Computer Science, Australian National University
 * Copyright 1999-2014
 */

#include <error.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h> 
#include <libgen.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <stdarg.h>
#include <sys/stat.h>
#include <sys/time.h>  
#include <sys/un.h>
#include <sys/wait.h>

#include <openssl/ssl.h>
#include <openssl/err.h>

#define DEBUG

#include "../util.h"
#include "ssl_authd.h"
#include "config.h"

#define MAX_MSG_LEN 1024
#define UNIX_PATH_LEN 108
#define DEFAULT_SOCKPATH "/var/run/ssl_authd_sock"

struct config conf;
struct connection connection;
char *program_name;
int dbglvl = LOG_ERR;

void connection_init (void) {
	connection.authenticated = 0;
	connection.pids.next = NULL;
	connection.fd = -1;
}

int connection_active_count (void) {
	return ((connection.fd != -1) && connection.authenticated);
}

void connect_ssl (SSL_CTX *ssl_ctx) {
	struct sockaddr sa;
	int retval;    

	connection.ssl = NULL;
	sa.sa_family = AF_INET;
	connection.fd = tcp_connect (get_sockaddr (&sa, config.server), config.port);
	if (connection.fd == -1) {
		debug_msg (LOG_ERR, "could not TCP connect");
		return;
	}
	debug_msg (LOG_DEBUG, "TCP connection up");
	connection.ssl = SSL_new (ssl_ctx);
	SSL_set_fd (connection.ssl, connection.fd);
	debug_msg (LOG_DEBUG, "FD set");
	retval = SSL_connect (connection.ssl);
	debug_msg (LOG_DEBUG, "SSL connected retval: %d", retval);
	if (retval == -1) {
		debug_msg (LOG_ERR, "SSL connect failed");
		//ERR_print_errors_fp (stderr);
		SSL_free (connection.ssl);
		connection.ssl = NULL;
		close (connection.fd);
		connection.fd = -1;
		return;
	}
	debug_msg (LOG_DEBUG, "SSL connection UP");
} /* connect_ssl () */

int check_userpass (char *user, char *pass) {
	debug_msg (LOG_DEBUG, "check_userpass, user %s, pass %s", user, pass);
	if (connection.fd == -1) {
		debug_msg (LOG_DEBUG, "check_userpass no match");
		return 0;
	}

	debug_msg (LOG_DEBUG, "table entry has user %s and pass %s",
		connection.user, connection.pass);
	if ((strncmp (connection.user, user, sizeof connection.user) == 0) && 
		(strncmp (connection.pass, pass, sizeof connection.pass) == 0)) {
		debug_msg (LOG_DEBUG, "check_userpass success");
		return 1;
	}
	return 0;
}

void add_pid (pid_t pid) {
	struct pidlist *p = &connection.pids;

	debug_msg (LOG_DEBUG, "adding pid %d to pid_list", pid);
	while (p->next != NULL) {
		if (p->pid == pid) return;
		p = p->next;
	}
	p->next = malloc (sizeof (struct pidlist));
	p = p->next;
	p->next = NULL;
	p->pid = pid;
} /* add_pid () */

void trash_pids (void) {
	struct pidlist *p;

	while (connection.pids.next) {
		p = connection.pids.next;
		connection.pids.next = p->next;
		free (p);
	}
} /* trash_pids () */

void remove_pid (pid_t pid) {
	struct pidlist *p = &connection.pids;
	struct pidlist *q;

	while ((p->next != NULL) && (p->next->pid != pid)) {
		debug_msg (LOG_DEBUG, "pid list has %d on it", p->next->pid);
		p = p->next;
	}
	if (p->next == NULL) {
		debug_msg (LOG_WARNING, "couldn't find pid %d on pidlist!", pid);
		return;
	}

	q = p->next;
	p->next = p->next->next;
	free (q);
	debug_msg (LOG_DEBUG, "removed pid %d from pid_list", pid);
} /* remove_pid () */

int new_auth (int fd, SSL_CTX *ssl_ctx, pid_t npid, char *user, char *pass) {
	char message[MAX_MSG_LEN];
	int nread;
	int retval;

	if (!user) return 0;
	if (!pass) return 0;
	if (strlen (user) > MAX_USER) user[MAX_USER] = 0;
	if (strlen (pass) > MAX_PASS) pass[MAX_PASS] = 0;

	if (check_userpass (user, pass) == 1) {
		if (write (fd, "SUCCESS\n", 8));
		add_pid (npid);
		return 1;
	}

	if (connection.fd != -1) return 0;		// HMMMM
	strcpy (connection.user, user);
	strcpy (connection.pass, pass);

	connect_ssl (ssl_ctx);
	if (connection.ssl == NULL) return 0;

	snprintf (message, sizeof(message), "%s\t%s\n", user, pass);
	retval = SSL_write (connection.ssl, message, strlen (message));
	debug_msg (LOG_INFO, "SSL_write returned %d", retval);
	nread = SSL_read (connection.ssl, message, sizeof(message));
	debug_msg (LOG_INFO, "SSL_read returned %d bytes", nread);
	while (nread > 0) {
		message[nread] = 0;
		debug_msg (LOG_DEBUG, "SSL returned %s", message);
		if (strncmp (message, "SUCCESS", 7) == 0) {
			/* run the SESSION_OPEN script */
			if (connection_active_count () == 0) {
				char command[MAX_MSG_LEN];
	
				snprintf (command, sizeof(command), "%s %s", 
					SESSION_OPEN_SCRIPT, connection.user);
				debug_msg (LOG_DEBUG, "about to run %s", command);
				if (system (command));
				debug_msg (LOG_DEBUG, "Open Script run");
			}
			add_pid (npid);
			connection.authenticated = 1;
			debug_msg (LOG_DEBUG, "Server message: %s", message);
			if (write (fd, message, nread));
			break;
		}
		debug_msg (LOG_INFO, "Server message: %s", message);
		if (write (fd, message, nread));
		nread = SSL_read (connection.ssl, message, sizeof(message));
	}

	if (connection.authenticated == 0) {
		SSL_shutdown (connection.ssl);
		SSL_free (connection.ssl);
		close (connection.fd);
		connection.fd = -1;
		trash_pids ();
	}
	return 1;
} /* new_auth () */

int sess_close (int fd, pid_t pid, char *user) {
	debug_msg (LOG_DEBUG, "closing session for user %s (connection user is %s)", user, connection.user);
	if (strcmp (connection.user, user) == 0) {
		debug_msg (LOG_INFO, "about to remove pid entry for %d for user %s", pid, user);
		remove_pid (pid);
	}
	if (write (fd, "SUCCESS\n", 8));
	return 1; 
} /* sess_close () */

int connection_new (int udsock, SSL_CTX *ssl_ctx) {
	char line[MAX_MSG_LEN];
	int nread;
	char *pid, *facility, *user;
	pid_t npid;
	int fd;

	debug_msg (LOG_DEBUG, "new connection on socket %d", udsock);

//	if ((fd = accept (udsock, (struct sockaddr *)&saddr, &addrlen)) == -1) {
	if ((fd = accept (udsock, 0, 0)) == -1) {
		debug_msg (LOG_ERR, "accept error");
		return -1;
	}
	nread = read (fd, line, sizeof (line));
	debug_msg (LOG_INFO, "new connection from pam_daemon - read: %d bytes", nread);
	if (nread < 0) goto done;
	line[nread] = 0;
	pid  = strtok (line, "\t");
	facility = strtok (NULL, "\t");
	user = strtok (NULL, "\t\n");

	if (!pid || !facility || !user || !strlen (pid) ||
		!strlen (facility) || !strlen (user)) {
		debug_msg (LOG_ERR, "one or more args is blank...");
		goto done;
	}
	debug_msg (LOG_INFO, "Pid: %s, facility: %s, user: %s", pid,
			facility, user);
	npid = atoi (pid);
	if (strcmp (facility, "auth") == 0) {
		new_auth (fd, ssl_ctx, npid, user, strtok (NULL, "\n\t"));
	} else if (strcmp (facility, "open") == 0) {
		if (write (fd, "SUCCESS\n", 8));
	} else if (strcmp (facility, "close") == 0) {
		sess_close (fd, npid, user);
	} else {
		debug_msg (LOG_ERR, "new connection not 'auth', 'open' or 'close'");
	}
done:
	debug_msg (LOG_DEBUG, "hit the magic done label\n");
	if (fd != -1) {
    		close (fd);
	}
	return 0;
} /* connection_new () */

void connection_remove (void) {
	char command[MAX_MSG_LEN];

	debug_msg (LOG_DEBUG, "running session close script");
	snprintf (command, sizeof(command), "%s %s", SESSION_CLOSE_SCRIPT, 
			connection.user);
	if (system (command));
	debug_msg (LOG_INFO, "Close script run");
	SSL_free (connection.ssl);
	close (connection.fd);
	connection.fd = -1;
	connection.authenticated = 0;
	trash_pids ();
	debug_msg (LOG_DEBUG, "Removed connection");
} /* connection_remove () */

/*
 * run through list of processes authenticated with us and remove any that have died
 */
void remove_dead_process () {
	char file[128];
	struct stat st;
	struct pidlist *p, *p2;

	p = connection.pids.next;
	while (p != NULL) {
		p2 = p->next;
		snprintf (file, sizeof(file), "/proc/%d", p->pid);
		if (stat (file, &st) == -1) {
			debug_msg (LOG_INFO, "Process %d died", p->pid);
			remove_pid (p->pid);
		}
		p = p2;
	}
} /* remove_dead_process () */

void main_loop (int udsock, SSL_CTX *ssl_ctx) {
	fd_set rfds;
	struct timeval tv;
	int retval;

	if (listen (udsock, 5) == -1) {
		debug_msg (LOG_ERR, "listen error");
		return;
	}

	/* setup the set and include the listen socket */
	while (1) {
		struct timeval *ptv;

		FD_ZERO(&rfds);
		FD_SET(udsock, &rfds);
		ptv = NULL;
		if (connection.authenticated) {
			/* set the (three second) time out */
			tv.tv_sec = 3;
			tv.tv_usec = 0;
			ptv = &tv;
		}

		if (select (FD_SETSIZE, &rfds, NULL, NULL, ptv)) {
			/* check for a new connection */
			if (FD_ISSET(udsock, &rfds))
			       	connection_new (udsock, ssl_ctx);
		} else { /* timeout */
			debug_msg (LOG_DEBUG, "Processing timeout");
			remove_dead_process ();
			if (connection.pids.next == NULL) {
				connection_remove ();
			} else {
				debug_msg (LOG_DEBUG, "Writing heartbeat...");
				retval = SSL_write (connection.ssl, "\n", 1);
				if (retval == -1) {
					debug_msg (LOG_INFO, "removing connection due to SSL write error");
					connection_remove ();
				}
			}
		}
	}
} /* main_loop () */

void usage (char *program, char *message) {
	fprintf (stderr, "%s\n", message);
	fprintf (stderr, "usage : %s [-c <config-file>] [-d] [-h] [-P]\n", program );
	fprintf (stderr, "\t-c <config-file>\tread configuration from <config-file>\n");
	fprintf (stderr, "\t-d \t\trun in debug (=non-daemon) mode\n");
	fprintf (stderr, "\t-P \t\tprint configuration on stdout and exit\n");
	fprintf (stderr, "\t-h \t\tthis message\n");
	fprintf (stderr, "\t-v \t\tincrease verbosity of error messages\n");
}  /* usage () */

int get_options (int argc, char *argv[], char *sockpath) {
	char c = 0;
	int want_printout = 0;
  
	config_load (DEFAULT_CONFIG_FILE);
	while ((c = getopt (argc, argv, "c:dhPv")) != EOF) {
		switch (c) {
			case 'c': config_load (optarg); break;
			case 'd': config.daemon_mode = 0; break;
			case 'P': want_printout = 1; break;
			case 'v': dbglvl++; break;
			default: usage (program_name, ""); return -1;
		}
	}
 
	if (want_printout) {
		config_print ();
		exit (0);
	}  
	return 0;
} /* get_options () */

SSL_CTX *setup_ssl () {
	SSL_CTX *ssl_ctx;

	SSLeay_add_ssl_algorithms ();
	SSL_load_error_strings ();
	ssl_ctx = SSL_CTX_new (SSLv23_client_method ());
	if (!ssl_ctx) {
		debug_msg (LOG_ERR, "%s", ERR_error_string (ERR_get_error (), NULL));
	}
	return (ssl_ctx);
} /* setup_ssl */

int main (int argc, char *argv[]) {
	int udsock;
	SSL_CTX *ssl_ctx;
	char sockpath[UNIX_PATH_LEN] = DEFAULT_SOCKPATH;

	program_name = rindex (argv[0], '/');
	program_name = program_name ? program_name + 1 : argv[0];

	/* get commandline options, load config if needed. */
	if (get_options (argc, argv, sockpath) < 0) {
		exit (EXIT_FAILURE);
	}

	if ((config.server == 0) || (config.port <= 0)) {
		error (1, 0, "Server or port not valid: %s, %d", config.server, config.port);
	}

	/* setup the domain socket */
	if ((udsock = uds_listen (sockpath, 0666, 3)) == -1) {
		exit (EXIT_FAILURE);
	}

	/* initialise the SSL libraries */
	if ((ssl_ctx = setup_ssl ()) == NULL) {
		exit (EXIT_FAILURE);
	}

	debug_msg (LOG_INFO, "Using server: %s, port %d and listening on %s",
		config.server, config.port, sockpath);

	signal (SIGPIPE, SIG_IGN);

	connection_init ();

	if (config.daemon_mode) {
		if (daemon (0, 0));
		openlog (program_name, LOG_PID + (config.daemon_mode ? 0 : LOG_PERROR),
			LOG_AUTHPRIV);
		tolog (&stderr);
	}

	main_loop (udsock, ssl_ctx);

	return EXIT_SUCCESS;
} /* main () */
