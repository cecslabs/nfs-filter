Summary: NFS home directory filter service client.
Name: nfs_filter_client
Version: 0.2
Release: 2
Copyright: GPL
Group: System Environment/Daemons
Source: http://tux.anu.edu.au/~matt/nfs_filter_client-0.2.tar.gz
URL: http://tux.anu.edu.au/~matt/
Packager: Matty <mattpratt@yahoo.com>
BuildRoot: /var/tmp/%{name}-buildroot
Requires: nss_ldap >= 113
Requires: openssl >= 0.9.5a

%description
The NFS filter packages offer a session based filtering infrastructure to help
secure home dirctory access on publicly accessible machines.
The client package contains a daemon and a PAM module that allows the host to
authenticate against, and use the services of a NFS filter server.
 
%prep
%setup

%build
make RPM_OPT_FLAGS="$RPM_OPT_FLAGS"
 
%install
rm -rf $RPM_BUILD_ROOT
make ROOT="$RPM_BUILD_ROOT" install

%clean
rm -rf $RPM_BUILD_ROOT
 
%files
%defattr(-,root,root)
#%doc README
 
/sbin/nfs_authd
/bin/nfs_login
/etc/rc.d/init.d/nfs_authd
/lib/security/pam_nfs_filter.so

%config 
/etc/nfs_filter/session_open.sh
/etc/nfs_filter/session_close.sh
/etc/nfs_filter/nfs_authd.conf
/etc/pam.d/system-auth
/etc/loginusers
/etc/shells

%post 
chkconfig --add nfs_authd
/etc/rc.d/init.d/nfs_authd restart
ln -fs /bin/tcsh /usr/local/bin/stutcsh

%changelog







