#ifndef __SSL_AUTHD_H
#define __SSL_AUTHD_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h> 
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <syslog.h>
#include <sys/stat.h>
#include <fcntl.h> 

#include <openssl/ssl.h>
#include <openssl/err.h>

#define BASE_DIR "/etc/ssl_authd"

#define DEFAULT_CONFIG_FILE BASE_DIR"/ssl_authd.conf"

#define MAX_CONNECTIONS 10
#define MAX_USER 15
#define MAX_PASS 63

#define SESSION_OPEN_SCRIPT  BASE_DIR"/session_open.sh"
#define SESSION_CLOSE_SCRIPT BASE_DIR"/session_close.sh"

struct pidlist {
    struct pidlist *next;
    pid_t pid;
};

struct connection {
    char user[MAX_USER + 1];
    char pass[MAX_PASS + 1];
    int authenticated;
    int fd;
    SSL *ssl;
    struct pidlist pids;
};

#endif /* __SSL_AUTHD_H */
