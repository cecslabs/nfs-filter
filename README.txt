NFS Filter project

NFS Filter is a project started at ANU Dept. of Computer Science in 1999 to build an iptables filter module to help protect Network File System (NFS) file accesses to an NFS server from "untrusted" clients.

NFS Filter has been successfully deployed in a number of Linux computer labs at ANU Dept. of Computer Science for over 10 years, with typically over 100 client machines accessing a single NFS home directory server.

How it works

The NFS Filter model is that a secure router (also referred to as a "server") performing Network Address Translation (NAT) sits between the relatively untrusted client machines and the real NFS server.

The router machine has the NFSFILT iptables kernel module installed as well as some ancilliary code to administer access to the server. NFS v2 over UDP only is supported, and, more recently, with only 1k read and write block sizes, which is quite small. Our experience is that file access times in general are not adversely affected by such small block sizes.

The NFSFILT iptables kernel module examines all NFS requests coming through the router and checks the user credentials (UID and GIDs) against tables of known good credentials for the users of each client machine. If any credentials are wrong, NFSFILT replaces all credentials for that request with those of the benign user "nobody" in group "nogroup". This replacement operation makes NFSFILT a "mangle" table module, as it can "mangle" packets passing through the router. The request is allowed to continue to the server so that the NFS protocol is not "broken" by simply dropping the request. The server is trusted to "do the right thing" with requests from "nobody/nogroup".

Organisation

NFS Filter is organised into two main subsystems:
 - the NFSFILT kernel module proper and associated patches for the iptables user-space command
 - a client/server authentication mechanism to dynamically configure valid user credentials

NFS Filter module

The sources for the main NFS Filter module are in:
 - server/kernel-module
 - server/iptables

The kernel-module can be built stand-alone against a current set of Linux kernel headers.

The iptables extensions to all NFSFILT to be a valid iptables "target" in the rules, can be added to the extensions directory the iptables source tree and then rebuild iptables.

Once installed and set up, individual per-user/per-IP_address rules can be added to /proc/net/nfs_filter/ip-add, from which they will appear in /proc/net/nfs_filter.

Client/Server Authentication mechanism

To add the per-user/per-IP_address rules in dynamically as users log in, a client-server authentication mechanism was developed.

The sources for the authentication mechanism are in:
 - server/daemon
 - client


